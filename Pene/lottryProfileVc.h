//
//  lottryProfileVc.h
//  LotteryApp
//
//  Created by katoch on 08/02/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"



@interface lottryProfileVc : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
    NSMutableArray*listArray;
    NSMutableArray*imagesArray;
    
    id profileData;
    
    
    NSString*email;
    NSString*Mobile;
    NSString*Utype;
    NSString*dob;
    
    NSString*aadhaar;
  
    NSString *Operator ;
    
    UIImage*checqImage;
    BOOL isCamera ;
  
    id imagedata;
    
    NSString*userid;

    
    
}




@property (strong, nonatomic) UIImage *imgProf;
@property (strong, nonatomic) IBOutlet UILabel *txtName;

@property (strong, nonatomic) IBOutlet UIAlertController *AlertCtrl;
@property (strong, nonatomic) IBOutlet UIButton *btnUploadingImG;

@property (strong, nonatomic) IBOutlet UIView *profileView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) IBOutlet UIView *imgView;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backClicked:(id)sender;
- (IBAction)updateBtn:(id)sender;
- (IBAction)cameraClicked:(id)sender;

@end
