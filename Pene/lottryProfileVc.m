//  lottryProfileVc.m
//  LotteryApp
//
//  Created by katoch on 08/02/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.

#import "lottryProfileVc.h"
#import <QuartzCore/QuartzCore.h>
#import "sideMenuCell.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "UpdateVc.h"


@interface lottryProfileVc ()
@end


@implementation lottryProfileVc


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setAlertCtrl];
    
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone ;
    
    _btnEdit.layer.borderWidth = 1.0f;
    _btnEdit.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _btnEdit.layer.cornerRadius = 5.0f;
    _btnEdit.clipsToBounds = YES;
    
    _profileImage.layer.cornerRadius = self.profileImage.frame.size.width/2 ;
    
    _imgView.layer.cornerRadius = self.imgView.frame.size.width/2 ;
    _imgView.layer.masksToBounds = YES;

    [KVNProgress show];
    [self performSelectorOnMainThread:@selector(profileApiFromServer) withObject:nil waitUntilDone:YES];
   

    
//    UIColor *rightColor = [UIColor colorWithRed:255.0/255.0 green:98.0/255.0 blue:116.0/255.0 alpha:1.0];
//    UIColor *middleColor = [UIColor colorWithRed:253/255.0 green:164.0/255.0 blue:155.0/255.0 alpha:1.0];
//    UIColor *leftColor = [UIColor colorWithRed:251.0/255.0 green:197.0/255.0 blue:168.0/255.0 alpha:1.0];
//
//    // Create the gradient
//    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
//    theViewGradient.colors = [NSArray arrayWithObjects: (id)leftColor.CGColor,(id)middleColor.CGColor ,(id)rightColor.CGColor, nil];
//    theViewGradient.frame = self.profileView.bounds;
//
//    //Add gradient to view
//    [self.profileView.layer insertSublayer:theViewGradient atIndex:0];
    
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
     [self performSelectorOnMainThread:@selector(updateImageApiToServer) withObject:nil waitUntilDone:YES];
    
   
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return listArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55 ;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
    if (cell==nil) {
        NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
        
        cell= arr[0];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.txtMenu.text  = [NSString stringWithFormat:@"%@",[listArray objectAtIndex:indexPath.row]];
    cell.menuImg.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    
    return cell;
    
}


- (IBAction)backClicked:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)updateBtn:(id)sender {
    
    UpdateVc *play = [[UpdateVc alloc]init];
    play = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdateVc"];
    play.mobileNumber = Mobile ;
    play.emailId = email ;
    play.dateOb =  dob ;
    
    play.firstName = [profileData valueForKey:@"firstNm"];
     play.secondName = [profileData valueForKey:@"lastNm"];
    play.type = [profileData valueForKey:@"userType"];
    [self showViewController:play sender:self];

    
}


- (IBAction)cameraClicked:(id)sender {
    
       [self requestAuthorizationWithRedirectionToSettings];
    
}


-(void)profileApiFromServer{
    
    NSString* userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/%@",userid];
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        profileData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(profileMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
-(void)profileMessage
{
    
    
    
    NSString*str = [NSString stringWithFormat:@"%@ %@",[profileData valueForKey:@"firstName"] ,[profileData valueForKey:@"lastName"]];
    
//    _lblName.text = str ;
    
    NSString*strDob = [profileData valueForKey:@"dob"];
    double valueDob = [strDob doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(valueDob / 1000.0)];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd-MM-yyyy"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:date]; // Convert date to string
    
    dob = dateStr ;
    
    email = [profileData valueForKey:@"email"];
    Mobile = [profileData valueForKey:@"mobile"];
    
    NSString*name = [profileData valueForKey:@"firstNm"];
    NSString*lastNm = [profileData valueForKey:@"lastNm"];
    
    NSString*FullName = [NSString stringWithFormat:@"%@ %@",name, lastNm];
    _txtName.text = FullName ;
    
    
    Utype =   [profileData valueForKey:@"userType"];
    
    Operator = [profileData valueForKey:@"oprt"];
   
    
    listArray = [[NSMutableArray alloc]initWithObjects:email,Mobile,Operator,dob,Utype, nil];
    
    imagesArray = [[NSMutableArray alloc]initWithObjects:@"envelope",@"mobile-phone",@"UserIcon",@"small-calendar",@"UserIcon", nil];
    
    [[SDImageCache sharedImageCache]clearMemory];
    [[SDImageCache sharedImageCache]clearDisk];
    
      [self.profileImage sd_setImageWithURL:[NSURL URLWithString:[profileData valueForKey:@"prflUrl"]]placeholderImage:[UIImage imageNamed:@"no-photo.jpg"]];
    
    [_tableView reloadData];
    
    
    
    
}

-(void)kvnDismiss{
    
    [KVNProgress dismiss];
    
}


///upload

-(void)setAlertCtrl;
{
    self.AlertCtrl = [UIAlertController alertControllerWithTitle:@"selectImage" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             
                             {
                                 [self camera];
                                 
                                 isCamera = true;
                                 
                                 
                             }];
    
    UIAlertAction *Library  = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action)
                               
                               {
                                   
                                   [self selectPhoto];
                                   
                                   isCamera = false;
                               }];
    
    UIAlertAction *Cancel  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        isCamera = false;
        
    }];
    
    [self.AlertCtrl addAction:camera];
    [self.AlertCtrl addAction:Library];
    [self.AlertCtrl addAction:Cancel];
    
}

- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

-(void)camera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        
    } else {
        
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
   
   
    
    if (isCamera == true) {
        
        
        
        checqImage = info[UIImagePickerControllerEditedImage];
        
        
        _profileImage.image = checqImage ;
        
       
        //        NSData *webData = UIImagePNGRepresentation(checqImage);
        //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //        NSString *documentsDirectory = [paths objectAtIndex:0];
        //
        //        self.txtCancelledCheck.text = documentsDirectory ;
        UIImageWriteToSavedPhotosAlbum(checqImage, nil, nil, nil);
        
      
       
        
        
    }else
    {
        
        
        checqImage = info[UIImagePickerControllerEditedImage];
        
        
        
        _profileImage.image = checqImage ;
        
        
      
        
        NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        //        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
        //        NSString *filename = [[result firstObject] filename];
        //        self.txtCancelledCheck.text = filename ;
        //
        
    }
    
    
     [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
//  [self performSelectorOnMainThread:@selector(updateImageApiToServer) withObject:nil waitUntilDone:YES];
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (void)requestAuthorizationWithRedirectionToSettings {
    dispatch_async(dispatch_get_main_queue(), ^{
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusAuthorized)
        {
            isCamera = false ;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                UIPopoverPresentationController *popPresenter = [_AlertCtrl popoverPresentationController];
                
                popPresenter.sourceView = self.btnUploadingImG;
                
                popPresenter.sourceRect = self.btnUploadingImG.bounds;
                
                [self presentViewController:_AlertCtrl animated:YES completion:nil];
                
            }else{
                
                [self presentViewController:self.AlertCtrl animated:YES completion:nil];
                
            }
            
        }
        else
        {
            //No permission. Trying to normally request it
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status != PHAuthorizationStatusAuthorized)
                {
                    //User don't give us permission. Showing alert with redirection to settings
                    //Getting description string from info.plist file
                    NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:cancelAction];
                    
                    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                    }];
                    [alertController addAction:settingsAction];
                    
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                }
            }];
        }
    });
}



-(void)updateImageApiToServer {
    
    
    NSString* Identifie = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", Identifie);
    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    
    NSString *urlString=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/pic"];
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    
    [_params setObject:userid forKey:@"usrId"];
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    NSString* profileKey = @"prflUrl";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSData *profiledata = UIImageJPEGRepresentation(checqImage, 1.0);
    
    if (profiledata) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", profileKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:profiledata];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
 
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      
                                      [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
                                      
                                      
                                      NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                      
                                      
                                      if(data!=nil){
                                          imagedata =  [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
                                          
                                          NSLog(@"%@",imagedata);
                                          
                                          [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
                                          
                                      }else{
                                          
                                          UIAlertController * alert = [UIAlertController
                                                                       alertControllerWithTitle:nil
                                                                       message:@"Error From Server"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction* noButton = [UIAlertAction
                                                                     actionWithTitle:@"Ok"
                                                                     style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         
                                                                     }];
                                          
                                          [alert addAction:noButton];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                      
                                      
                                      
                                      
                                      
                                  }];
    
    [task resume];
    
    
}



-(void)checkResponse{

    
    if ([[imagedata valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]])
    {
        
        [self profileApiFromServer];
        
    }
      
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[imagedata valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}



@end
