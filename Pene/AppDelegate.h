//
//  AppDelegate.h
//  Pene
//
//  Created by katoch on 01/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    id versionCheck ;
}

@property (strong, nonatomic) UIWindow *window;


- (void)saveContext;

@end

