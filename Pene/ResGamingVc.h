//
//  ResGamingVc.h
//  Pene
//
//  Created by katoch on 06/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "sideMenuCell.h"
#import "PlayVc.h"
#import "AboutVc.h"
#import "ContactVc.h"
#import "SideHistoryVc.h"
#import "PrivcyVc.h"
#import "TermsVc.h"
#import "FaqVc.h"
#import "lottryProfileVc.h"
#import "SupportVc.h"
#import "AppDelegate.h"
#import "loginVc.h"
#import "UIImageView+WebCache.h"



@interface ResGamingVc : UIViewController<UIGestureRecognizerDelegate>{
    DropDownView *dropDownView ;
    NSMutableArray*menuItemsSection1;
    NSMutableArray*sectionImages1;
    
    id profileData;
    NSString *userid ;
    NSString*credits;
    
}

@property (strong, nonatomic) UITextField *dropDownTxtfield;


@property (strong, nonatomic) IBOutlet UIView *Viewimage;
@property (strong, nonatomic) IBOutlet UIImageView *SideImage;
@property (strong, nonatomic) IBOutlet UITableView *sideTableView;
@property (strong, nonatomic) IBOutlet UILabel *txtSideName;
@property (strong, nonatomic) IBOutlet UILabel *txtSideEmail;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSlide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
- (IBAction)menuClicked:(id)sender;


@end
