//
//  main.m
//  Pene
//
//  Created by katoch on 01/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
