//
//  ResGamingVc.m
//  Pene
//
//  Created by katoch on 06/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "ResGamingVc.h"
#import "HistoryVc.h"
#import <AFNetworking.h>
#import "KVNProgress.h"

@interface ResGamingVc ()

@end

@implementation ResGamingVc

- (void)viewDidLoad {
    [super viewDidLoad];
    [_sideTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    _SideImage.layer.cornerRadius = self.SideImage.frame.size.width/2 ;
    _SideImage.layer.masksToBounds = YES;
    
    _Viewimage.layer.cornerRadius = self.Viewimage.frame.size.width/2 ;
    _Viewimage.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    menuItemsSection1 = [[NSMutableArray alloc]initWithObjects:@"Profile",@"About us",@"Contact us",@"History",@"Privacy Policy",@"Terms & Conditions",@"Logout",nil];
  sectionImages1 = [[NSMutableArray alloc]initWithObjects:@"avatar",@"aboutUs",@"contactUs",@"history-symbol-of-antique-building (3)",@"privacy-60",@"terms",@"aboutUs",@"logout-60", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _viewMenuSlide.hidden = true;
     userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    [self profileApiFromServer];
}

- (IBAction)menuClicked:(id)sender {
    
    
    [self.view bringSubviewToFront:self.viewMenuSlide];
    _viewMenuSlide.hidden = false;
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = 0;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        //            _viewMenuSlide.hidden = true;
        
    }];
    
}

-(void)tapGesture{
    
    [dropDownView closeAnimation];
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        _viewMenuSlide.hidden = true;
        
    }];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }
    else if ([touch.view isDescendantOfView:_sideTableView]) {
        
        
        
        return NO;
    }
    
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    
    return 1;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
     if (tableView== _sideTableView) {
        
        return menuItemsSection1.count;
        
    }
    else
    {
        
        return 0 ;
        
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _sideTableView) {
        
        
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            return 45;
        }else{
            return 65 ;
            
        }
        
        
    }
    return 0;
    
}
    




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    if (tableView== _sideTableView){
        sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
        
        cell.txtMenu.text  = [menuItemsSection1 objectAtIndex:indexPath.row];
        cell.menuImg.image = [UIImage imageNamed:[sectionImages1 objectAtIndex:indexPath.row]];
        
        
        
        
        return cell;
        
        
    }
    return 0;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_sideTableView) {
        
        if ([menuItemsSection1[indexPath.row] isEqualToString:@"Profile"]) {
            lottryProfileVc*tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"lottryProfileVc"];
            
            
            
            [self showViewController:tbc sender:self];
            
            
        }
        //        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"How to play"])
        //        {
        //            PlayVc *play = [[PlayVc alloc]init];
        //            play = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVc"];
        //
        //
        //
        //  [self showViewController:play sender:self];
        //
        //
        //        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"About us"])
        {
            AboutVc *About = [[AboutVc alloc]init];
            About = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutVc"];
            
            [self showViewController:About sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Contact us"])
        {
            ContactVc *contact = [[ContactVc alloc]init];
            contact = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactVc"];
            
            [self showViewController:contact sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"History"])
        {
            SideHistoryVc *his = [[SideHistoryVc alloc]init];
            his = [self.storyboard instantiateViewControllerWithIdentifier:@"SideHistoryVc"];
            his.checkStr = @"History";
            
            [self showViewController:his sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Privacy Policy"])
        {
            PrivcyVc *privac = [[PrivcyVc alloc]init];
            privac = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcyVc"];
            
            [self showViewController:privac sender:self];
            
        }else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Terms & Conditions"])
        {
            TermsVc *term = [[TermsVc alloc]init];
            term = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsVc"];
            
            [self showViewController:term sender:self];
            
        }
        //        else if (indexPath.row == 7)
        //        {
        //            FaqVc *About = [[FaqVc alloc]init];
        //            About = [self.storyboard instantiateViewControllerWithIdentifier:@"FaqVc"];
        //
        //            [self showViewController:About sender:self];
        //
        //        }
        
        
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Logout"]) {
            UIAlertView*logout = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure you want to log out" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            logout.tag = 11 ;
            logout.delegate = self;
            [logout show];
        }
        
    }
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag ==11){
        
        if (buttonIndex == 1) {
            
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            loginVc*lVc = [[loginVc alloc]init];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            lVc= [storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
            UINavigationController *navControllr = [[UINavigationController alloc]initWithRootViewController:lVc];
            navControllr.navigationBarHidden = true;
            delegate.window.rootViewController= navControllr;
            [delegate.window makeKeyAndVisible];
            
            
        }
    }
    
}


-(void)profileApiFromServer{
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/%@",userid];
    
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        
        
        profileData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(profileMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
-(void)profileMessage
{
    
    if ([[profileData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        NSString*str = [NSString stringWithFormat:@"%@ %@",[profileData valueForKey:@"firstNm"] ,[profileData valueForKey:@"lastNm"]];
        _txtSideName.text = str ;
        
        NSString* mobile = [profileData valueForKey:@"mobile"];
        
        _txtSideEmail.text = mobile ;
        
        credits = [NSString stringWithFormat:@"%@",[profileData valueForKey:@"credits"]];
        
        [self.SideImage sd_setImageWithURL:[NSURL URLWithString:[profileData valueForKey:@"prflUrl"]]placeholderImage:[UIImage imageNamed:@"no-photo.jpg"]];

        
        
        [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"UserName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
        
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[profileData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}
-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}


@end
