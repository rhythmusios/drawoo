//
//  ForgetPasswordVc.h
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPasswordVc : UIViewController{
    
    id passwordData;
    NSString*useriD;
    id  verifiedData  ;
    id resendData;
    
}

- (IBAction)backOtp:(id)sender;


@property (strong, nonatomic) IBOutlet UITextField *txtMobile;

- (IBAction)backClicked:(id)sender;
- (IBAction)verifyClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnVerify;

- (IBAction)verifyOtp:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnVerifyOtp;
@property (strong, nonatomic) IBOutlet UITextField *txtOtp;
@property (strong, nonatomic) IBOutlet UIView *viewOtp;
- (IBAction)resendClicked:(id)sender;

@end
