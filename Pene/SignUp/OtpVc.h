//
//  OtpVc.h
//  CityBus
//
//  Created by katoch on 19/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"


@interface OtpVc : UIViewController<UIGestureRecognizerDelegate>{
    id resendData;
    id verifiedData;
    
    NSString* userId;
}
- (IBAction)VarifyClicked:(id _Nullable )sender;
- (IBAction)resendOtpClicked:(id _Nullable )sender;
- (IBAction)backClicked:(id _Nullable )sender;


@property(strong,nonnull)NSString *otpStr;
@property(strong,nonnull)NSString *mobileStr;
@property(strong,nonnull)NSString *token;


@property (strong, nonatomic) IBOutlet UITextField * _Nullable txtOtp;
@property (strong, nonatomic) IBOutlet UIButton * _Nullable btnVarify;
@property (strong, nonatomic)NSString * _Nullable mobile;
@end
