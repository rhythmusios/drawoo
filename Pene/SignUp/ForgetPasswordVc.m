//
//  ForgetPasswordVc.m
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "ForgetPasswordVc.h"
#import "KVNProgress.h"
#import <AFNetworking/AFNetworking.h>
#import "CreatePasswordVc.h"
#import "KVNProgress.h"

@interface ForgetPasswordVc ()

@end

@implementation ForgetPasswordVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _btnVerifyOtp.layer.cornerRadius = 15.0f ;
    _btnVerifyOtp.layer.masksToBounds = YES;
    
    UIImageView *countryImage;
    
    UIView *userTypePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    countryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [countryImage setImage:[UIImage imageNamed:@"mobile-phone"]];
    [userTypePadding addSubview:countryImage];
    _txtMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtMobile.leftView = userTypePadding ;
    
    self.btnVerify.layer.cornerRadius = _btnVerify.frame.size.height/2 ;
    self.btnVerify.layer.masksToBounds = YES;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.viewOtp setBackgroundColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.7]];
    
    
    [self.viewOtp setHidden:YES];
    
   
    

}


- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)verifyClicked:(id)sender {
    if ([self.txtMobile.text isEqualToString:@""]) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your mobile number"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        
        [self  sendMobileApiFromServer];
        
    }
    
}



-(void)sendMobileApiFromServer{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr//frgt/%@",_txtMobile.text];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    //    NSString*consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"CosumerId"];
    
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         passwordData = responseObject;
         
         [self performSelectorOnMainThread:@selector(operatorResponse) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              UIAlertController * alert = [UIAlertController
                                           alertControllerWithTitle:nil
                                           message:@"Server Error"
                                           preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction* noButton = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             
                                         }];
              
              [alert addAction:noButton];
              [self presentViewController:alert animated:YES completion:nil];
              
              
          }];
    
}


-(void)operatorResponse{
    
    if ([[passwordData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        _txtOtp.text = @"";
        
        
        [self.viewOtp setHidden:NO];
        
        useriD = [passwordData valueForKey:@"usrId"];
        
       
        [[NSUserDefaults standardUserDefaults]setObject:useriD forKey:@"id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[passwordData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}



-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}
- (IBAction)verifyOtp:(id)sender {
    
    
    
    [KVNProgress show];
    [self verifyOtpFromApi];

    
}

/////////////// verify clicked api ////////////////////

-(void)verifyOtpFromApi{

    

    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/otp"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary* _params = [[NSDictionary alloc] init];

    _params = @{
                @"mobile" :_txtMobile.text,
                @"otp" :_txtOtp.text,
                @"usrId" :useriD,
                @"action" : @"Verify"

                };


    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];

         verifiedData = responseObject;
         [self performSelectorOnMainThread:@selector(goToPasswordView) withObject:nil waitUntilDone:YES];


     } failure:^(NSURLSessionTask *operation, NSError *error) {

         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];

         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Server error"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {

                                    }];

         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];



     }];



}

-(void)goToPasswordView {

    if ([[verifiedData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        CreatePasswordVc *Cvc = [[CreatePasswordVc alloc]init];
        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
        Cvc.checkForget =  @"reset";
        [self.navigationController pushViewController:Cvc animated:YES];

    }else{

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[verifiedData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void)kvnDismiss{
    [KVNProgress dismiss];
    
}


/////////////////// ResendClicked api ///////////////////////////////////////////////////

-(void)ResendOtpFromApi{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/otp"];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    
  
    
    _params = @{
                @"mobile" :_txtMobile.text,
                @"otp" :_txtOtp.text,
                @"usrId" :useriD,
                @"action" : @"Resend"
                
                };
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         resendData = responseObject;
         [self performSelectorOnMainThread:@selector(resendOtpResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Server Error"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
         
     }];
    
    
    
}

-(void)resendOtpResponse {
    
    if ([[resendData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        NSLog(@"success");
        
    }
    else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[resendData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}


- (IBAction)resendClicked:(id)sender {
    
   
    
    [KVNProgress show];
    [self ResendOtpFromApi];
}
- (IBAction)backOtp:(id)sender {
    
    [self.viewOtp setHidden:YES];
    
}
@end
