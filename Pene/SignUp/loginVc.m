//
//  loginVc.m
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "loginVc.h"
#import "ForgetPasswordVc.h"
#import "HomeVc.h"
#import <QuartzCore/QuartzCore.h>
#import <AFNetworking/AFNetworking.h>
#import "registerVc.h"
#import "HomeVc.h"
#import "KVNProgress.h"
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTSubscriber.h>
#import <CoreTelephony/CTSubscriberInfo.h>
#import <CoreTelephony/CTCellularData.h>
#import <stdlib.h>
#import "Reachability.h"


@interface loginVc ()

@end

@implementation loginVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _imgLogo.layer.cornerRadius = self.imgLogo.frame.size.width/2 ;
    _imgLogo.layer.masksToBounds = YES;
    
    _viewImg.layer.cornerRadius = self.viewImg.frame.size.width/2 ;
    _viewImg.layer.masksToBounds = YES;
    
    
//    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Don't have an account? SignUp"]];
//    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 22)];
//    [_btnRegister setAttributedTitle:attrStr forState:UIControlStateNormal];
    
    
    
    _txtUserType.text = @"Individual";
    
    UIView *dropDownPadding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    UIImageView *dropImage1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [dropImage1 setImage:[UIImage imageNamed:@"down-arrow"]];
    [dropDownPadding1 addSubview:dropImage1];
    _txtUserType.rightViewMode = UITextFieldViewModeAlways;
    _txtUserType.rightView = dropDownPadding1 ;
    
//    UIColor *rightColor = [UIColor colorWithRed:255.0/255.0 green:98.0/255.0 blue:116.0/255.0 alpha:1.0];
//    UIColor *middleColor = [UIColor colorWithRed:253/255.0 green:164.0/255.0 blue:155.0/255.0 alpha:1.0];
//    UIColor *leftColor = [UIColor colorWithRed:251.0/255.0 green:197.0/255.0 blue:168.0/255.0 alpha:1.0];
//
//    // Create the gradient
//    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
//    theViewGradient.colors = [NSArray arrayWithObjects: (id)leftColor.CGColor, (id)middleColor.CGColor,(id)rightColor.CGColor, nil];
//    theViewGradient.frame = self.view.bounds;
//    
    //Add gradient to view
//    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    
    UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [dropImage setImage:[UIImage imageNamed:@"drop-down-arrow.png"]];
    [dropDownPadding addSubview:dropImage];
    _userType.rightViewMode = UITextFieldViewModeAlways;
    _userType.rightView = dropDownPadding ;
    
    _userType.text = @"Individual";
     [_userType addTarget:self action:@selector(userTypeDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
     [self.firstImage setAlpha:1.0];
 dropDownArray = [[NSMutableArray alloc]initWithObjects:@"Individual",@"Agent", nil];
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    self.btnLogin.layer.cornerRadius = _btnLogin.frame.size.height/2 ;
    
    UIView *userTypePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
  UIImageView*  userTypeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [userTypeImage setImage:[UIImage imageNamed:@"UserIcon"]];
    [userTypePadding addSubview:userTypeImage];
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.leftView = userTypePadding ;
    
    
    UIView *userPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView*  userImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [userImage setImage:[UIImage imageNamed:@"UserIcon"]];
    [userPadding addSubview:userImage];
    _txtUserType.leftViewMode = UITextFieldViewModeAlways;
    _txtUserType.leftView = userPadding ;
    
    UIView *passwordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView*  passImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [passImage setImage:[UIImage imageNamed:@"PasswordIcon"]];
    [passwordPadding addSubview:passImage];
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.leftView = passwordPadding ;
    
//     UIColor *color = [UIColor colorWithRed:255.0f/255.0 green:230.0f/255.0 blue:230.0f/255.0 alpha:1
//                       ];
//    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile number" attributes:@{NSForegroundColorAttributeName: color}];
//    _txtUserType.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"UserType" attributes:@{NSForegroundColorAttributeName: color}];
//    _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    
    
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"pink"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        return NO;
        
    }
    
    
    return YES;
}
-(void)tapGesture{
    
    [_txtPassword resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_userType resignFirstResponder];
    
    [dropDownView closeAnimation];
    
}



- (IBAction)loginClicked:(id)sender {
    

    
    if ([_txtEmail.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter Mobile/Email ID/Swipeler ID"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
     else  if ([self.userType.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please select the UserType"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([self.txtPassword.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }

    else{

        
        
        
        [KVNProgress show];
        [self loginApiToServer];
    }

}

- (IBAction)registerClicked:(id)sender {
    
    registerVc *SignUp = [[registerVc alloc]init];
    SignUp = [self.storyboard instantiateViewControllerWithIdentifier:@"registerVc"];
    [self.navigationController pushViewController:SignUp animated:YES];
    
}

- (IBAction)forgetClicked:(id)sender {
    ForgetPasswordVc *ForgetPassword = [[ForgetPasswordVc alloc]init];
    ForgetPassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgetPasswordVc"];
    [self.navigationController pushViewController:ForgetPassword animated:YES];
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    

    
    if (textField == _txtUserType) {
        [dropDownView.view removeFromSuperview];
                [self.txtEmail resignFirstResponder];
        [self.view endEditing:YES];
        
        
        dropDownView = [[DropDownView alloc] initWithArrayData:dropDownArray cellHeight:40 heightTableView:90 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
        
       
        
        dropDownView.delegate = self;
        dropDownView.view.backgroundColor = [UIColor colorWithRed:106/255.0f green:27/255.0 blue:154/255.0 alpha:1];
        
        [self.loginView addSubview:dropDownView.view];
        
        [self.view bringSubviewToFront:dropDownView.view];
        _dropDownTxtfield = _txtUserType ;
        [ dropDownView openAnimation];
        return NO ;
        
        
    }
    else
    {
         return YES;
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [_txtEmail resignFirstResponder];
     [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    [textField resignFirstResponder];
    return YES ;
    
}


-(void)dropDownCellSelected:(NSInteger)returnIndex{
    
    if (_dropDownTxtfield == _txtUserType) {
        
        _txtUserType.text = [dropDownArray objectAtIndex:returnIndex];
        
    }
    
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     if (textField == _txtPassword) {
        if (textField.text.length == 15 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
     }

    
    return YES;
    
}

-(BOOL)isValidPassword:(NSString *)passwordString
{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:passwordString];
}

-(void)loginApiToServer{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil message: @"No Network Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
        
        
    }else{
    
    NSString* Identifie = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", Identifie);
    
    NSString *version = [[UIDevice currentDevice] systemVersion];

    
    
    NSString* strPassword= self.txtPassword.text;
//    NSData *dataPass = [strPassword dataUsingEncoding:NSUTF8StringEncoding];
//
//    // Convert to Base64 data
//    NSData *base64Data = [dataPass base64EncodedDataWithOptions:0];
//    NSString* Base64encodePassword = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:[base64Data bytes]]];
//
//    NSLog(@"%@", Base64encodePassword);

        NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/unsecure/usrlgn"];
        AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
        managerr.requestSerializer = [AFJSONRequestSerializer serializer];
//    NSString*consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    NSData *nsdata = [strPassword
                      dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    // Print the Base64 encoded string
    NSLog(@"Encoded: %@", base64Encoded);

    
    
        NSDictionary* _params = [[NSDictionary alloc] init];
    _params = @{
                
                @"mobile" :_txtEmail.text,
                @"usrTyp" :_txtUserType.text,
                @"pass" :base64Encoded
                
                };
    
    
    
    
        [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
         
         {
             NSLog(@"PLIST: %@", responseObject);
             [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
             
             loginData = responseObject;
             [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
             
             
         }
               failure:^(NSURLSessionTask *operation, NSError *error) {
             [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
                   UIAlertController * alert = [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:@"Server error"
                                                preferredStyle:UIAlertControllerStyleAlert];
                   UIAlertAction* noButton = [UIAlertAction
                                              actionWithTitle:@"Ok"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action) {
                                                  
                                              }];
                   
                   [alert addAction:noButton];
                   [self presentViewController:alert animated:YES completion:nil];
             
             
         }];
    
    }
    
    
}

-(void)checkResponse{
    
    
    NSString*UserType = _txtUserType.text ;
    
    [[NSUserDefaults standardUserDefaults]setObject:UserType forKey:@"UserType"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
        if ([[loginData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSString*mobile = [NSString stringWithFormat:@"%@",[loginData valueForKey:@"mobile"]];
            
             NSString*userId =[NSString stringWithFormat:@"%@",[loginData valueForKey:@"usrId"]];
            
            [[NSUserDefaults standardUserDefaults]setObject:userId forKey:@"id"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            UITabBarController *rootViewController = [[UITabBarController alloc] init];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
            
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            
            delegate.window.rootViewController = rootViewController ;
            [delegate.window makeKeyAndVisible];

//            UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
//             [self showViewController:tbc sender:self];
            
        }
        else{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:[loginData valueForKey:@"message"]
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
}

    
-(void)kvnDismiss{
        
        [KVNProgress dismiss];
    }
    
-(void)kvnStart{
        
        [KVNProgress show];
    
    }


//-(void)shakeLayer:(CALayer*)layer{
//    int repeats = 4;
//    float time = 0.04;
//    float movement = 5;
//    for(int x = 0; x < repeats; x++){
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(x*(time*4) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            CABasicAnimation *posAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
//            [posAnimation setFromValue:[NSNumber numberWithFloat:0]];
//            [posAnimation setToValue:[NSNumber numberWithFloat:movement]];
//            [posAnimation setBeginTime:0.0];
//            [posAnimation setDuration:time];
//
//            CABasicAnimation *negAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
//            [negAnimation setFromValue:[NSNumber numberWithFloat:movement]];
//            [negAnimation setToValue:[NSNumber numberWithFloat:-movement]];
//            [negAnimation setBeginTime:time];
//            [negAnimation setDuration:time*2];
//
//            CABasicAnimation *posAnimation2 = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
//            [posAnimation2 setFromValue:[NSNumber numberWithFloat:-movement]];
//            [posAnimation2 setToValue:[NSNumber numberWithFloat:0]];
//            [posAnimation2 setBeginTime:time*3];
//            [posAnimation2 setDuration:time];
//
//            CAAnimationGroup *group = [CAAnimationGroup animation];
//            [group setDuration:time*4];
//            [group setAnimations:[NSArray arrayWithObjects:posAnimation, negAnimation, posAnimation2, nil]];
//
//            [layer addAnimation:group forKey:nil];
//        });
//    }
//}

- (IBAction)dialClicked:(id)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:*121#"]];
    
}
@end
