//
//  CreatePasswordVc.m
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "CreatePasswordVc.h"
#import "HomeVc.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"

@interface CreatePasswordVc ()

@end

@implementation CreatePasswordVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.btnSubmit.layer.cornerRadius = _btnSubmit.frame.size.height/2 ;

    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    UIView *passwordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *passwordImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [passwordImage setImage:[UIImage imageNamed:@"PasswordIcon"]];
    [passwordPadding addSubview:passwordImage];
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.leftView = passwordPadding ;
    
    
    UIView *ConpasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *conpasswordImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [conpasswordImage setImage:[UIImage imageNamed:@"PasswordIcon"]];
    [ConpasswordPadding addSubview:conpasswordImage];
    _txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtConfirmPassword.leftView = ConpasswordPadding ;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
     userId = [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
}

-(void)tapGesture{
    [self.view endEditing:YES];
}

- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitClicked:(id)sender {
    
    NSString*aString;
    aString = _txtPassword.text ;

    
    NSCharacterSet *CaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
  NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    if ([self.txtPassword.text isEqualToString:@""]||self.txtPassword.text.length < 8) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Enter at least 8 characters of  password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else  if ([aString rangeOfCharacterFromSet:CaseChars].location == NSNotFound || [aString rangeOfCharacterFromSet:numbers].location == NSNotFound) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Password should contain atleast one character and numeric value"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }


    else if (![self.txtConfirmPassword.text isEqualToString:_txtPassword.text]){
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Passwords not matching"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];

    }else{

        
        if ([_checkForget isEqualToString:@"reset"]) {
            [KVNProgress show];
            [self ResetPasswordFromApi];
            
        }else{
        [KVNProgress show];
        [self sendPasswordFromApi];
            
        }
    }
}

-(BOOL)isValidPassword:(NSString *)passwordString
{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:passwordString];
}


-(void)sendPasswordFromApi{
    
   
    NSString* strPassword= self.txtPassword.text;
    
    NSData *nsdata = [strPassword
                      dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    // Print the Base64 encoded string
    NSLog(@"Encoded: %@", base64Encoded);
    
    
    
    
//    NSData *dataPass = [strPassword dataUsingEncoding:NSUTF8StringEncoding];
//
//    // Convert to Base64 data
//    NSData *base64Data = [dataPass base64EncodedDataWithOptions:0];
//   NSString* Base64encodePassword = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:[base64Data bytes]]];
//
//    NSLog(@"%@", Base64encodePassword);
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//unsecure/setpass"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString*token = [[NSUserDefaults standardUserDefaults]valueForKey:@"tokenID"];
    
    
    [managerr.requestSerializer setValue:token forHTTPHeaderField:@"token"];

    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"usrId" :userId,
                @"pswrd" :base64Encoded
               
                
                };
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
                     passwordData = responseObject;
         [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         NSLog(@"Error: %@", error);
         
         
         
     }];
   
}
-(void)checkResponse{
    
    if ([[passwordData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
       
        NSString*userId = [passwordData valueForKey:@"usrId"];
        [[NSUserDefaults standardUserDefaults]setValue:userId forKey:@"id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
       
        
        NSString*mobile = [NSString stringWithFormat:@"%@",[passwordData valueForKey:@"mobile"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
//        UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
//
//           [self showViewController:tbc sender:self];
        
        UITabBarController *rootViewController = [[UITabBarController alloc] init];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        delegate.window.rootViewController = rootViewController ;
        [delegate.window makeKeyAndVisible];



    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error From Server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)kvnDismiss{

    [KVNProgress dismiss];
}

-(void)kvnStart{
    
    [KVNProgress show];
}


-(void)ResetPasswordFromApi{
    
    
    NSString* strPassword= self.txtPassword.text;
    
    NSData *nsdata = [strPassword
                      dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    // Print the Base64 encoded string
    NSLog(@"Encoded: %@", base64Encoded);
    
    
    
    
    //    NSData *dataPass = [strPassword dataUsingEncoding:NSUTF8StringEncoding];
    //
    //    // Convert to Base64 data
    //    NSData *base64Data = [dataPass base64EncodedDataWithOptions:0];
    //   NSString* Base64encodePassword = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:[base64Data bytes]]];
    //
    //    NSLog(@"%@", Base64encodePassword);
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/setpass"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString*token = [[NSUserDefaults standardUserDefaults]valueForKey:@"tokenID"];
    
    
    [managerr.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"usrId" :userId,
                @"pswrd" :base64Encoded
                
                
                };
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         resetPasswordData = responseObject;
         [self performSelectorOnMainThread:@selector(resetResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         NSLog(@"Error: %@", error);
         
         
         
     }];
    
}
-(void)resetResponse{
    
    if ([[resetPasswordData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        NSString*userId = [resetPasswordData valueForKey:@"usrId"];
        [[NSUserDefaults standardUserDefaults]setValue:userId forKey:@"id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
        NSString*mobile = [NSString stringWithFormat:@"%@",[resetPasswordData valueForKey:@"mobile"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        //        UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        //
        //           [self showViewController:tbc sender:self];
        
        UITabBarController *rootViewController = [[UITabBarController alloc] init];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        delegate.window.rootViewController = rootViewController ;
        [delegate.window makeKeyAndVisible];
        
        
        
        
        
        
    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error From Server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

@end
