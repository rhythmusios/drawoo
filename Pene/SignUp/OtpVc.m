
//  Created by katoch on 19/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "OtpVc.h"
#import "HomeVc.h"
#import "CreatePasswordVc.h"

@interface OtpVc ()

@end

@implementation OtpVc

- (void)viewDidLoad {
    [super viewDidLoad];
   self.btnVarify.layer.cornerRadius = _btnVarify.frame.size.height/2 ;
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
     userId = [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
}

-(void)tapGesture{
    
    [_txtOtp resignFirstResponder];
    [self.view endEditing:YES];
   
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)VarifyClicked:(id)sender {
    
    [KVNProgress show];
    [self  verifyOtpFromApi];
    
//    CreatePasswordVc * viewStep = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
//
//    viewStep.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self.navigationController pushViewController:viewStep animated:YES];
   
}

- (IBAction)resendOtpClicked:(id)sender{
    
    [KVNProgress show];
    [self ResendOtpFromApi];
    
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}




/////////////// verify clicked api ////////////////////

-(void)verifyOtpFromApi{
    
//    "private String mobile;
//    private int otp;
//    private long usrId;
//    private String action; //Verify,Resend"
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//unsecure/otp"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobile" :_mobile,
                @"otp" :_txtOtp.text,
                @"usrId" :userId,
                @"action" : @"Verify"
                
                };
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         verifiedData = responseObject;
         [self performSelectorOnMainThread:@selector(goToPasswordView) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         
          [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Server error"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
         
         
         
     }];
    
    
    
}

-(void)goToPasswordView {

    if ([[verifiedData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        CreatePasswordVc *Cvc = [[CreatePasswordVc alloc]init];
        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
        
        [self.navigationController pushViewController:Cvc animated:YES];

    }else{

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[verifiedData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

/////////////////// ResendClicked api ///////////////////////////////////////////////////

-(void)ResendOtpFromApi{
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//unsecure/otp"];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobileNo" :_mobile,
                @"regotp" :_txtOtp.text,
                @"usrId" :userId,
                @"action" : @"Resend"
                
                };
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         resendData = responseObject;
         [self performSelectorOnMainThread:@selector(resendOtpResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Server Error"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
      
     }];
    
    
    
}

-(void)resendOtpResponse {
    
    if ([[resendData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        NSLog(@"success");
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[resendData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}


-(void)kvnDismiss{
    [KVNProgress dismiss];

}

@end
