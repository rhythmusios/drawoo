//
//  HomeVc.h
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DropDownView.h"
#import "PlayVc.h"
#import "AboutVc.h"
#import "ContactVc.h"
#import "SideHistoryVc.h"
#import "PrivcyVc.h"
#import "TermsVc.h"
#import "FaqVc.h"
#import "SupportVc.h"
#import <AFNetworking/AFNetworking.h>




@interface HomeVc : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate,DropDownViewDelegate,UIScrollViewDelegate>{
    NSMutableDictionary* dicData;
    NSMutableArray*menuItemsSection1;
    NSMutableArray *swipelerTitle ;
    NSMutableArray*txtDetails;
    NSString*strTitle ;
    NSMutableArray*menuItemsSection2;
    NSMutableArray*sectionImages1;
    NSMutableArray*sectionImages2;
    
    NSMutableArray*merchantTitleArray;
    BOOL checkMerchant;
    
    NSMutableArray*adsUrl;
    NSTimer * timer ;
    NSMutableArray* result ;
    NSMutableArray*adsArray;
    NSMutableArray *operatorArray;
    NSMutableString *textStr;
   DropDownView *dropDownView ;
    NSMutableArray *lotteryArray;
    
    NSMutableArray*slidingArray;
    
    id operatorData;
    BOOL Checkopertaor ;
    
    id drawData;
    NSMutableArray*bidAmount ;
    NSMutableArray*TemplIds ;
      NSMutableArray*templName ;
    NSMutableArray*drawTimeArray;
    NSInteger dtvalue;
    NSInteger endvalue;
    
    NSString *checkDateStr ;
    NSMutableArray*adImgUrl;
    id profileData;
    
    NSMutableArray *prizeAarray;
    NSMutableArray*hourlyAmntArray;
    NSMutableArray *dailyAmntArray;
    NSMutableArray *monthlyAmntArray;
    
    id bidData;
    NSString* userid ;
    NSString*tempId;
    NSString*livTempId;
    NSString *mobile ;
    NSString*bidAmountStr;
    
    NSMutableArray*imagesUrlArray;
    NSMutableArray *btnPeneArray ;
    NSMutableArray*istPrizeArray;
    NSMutableArray*secondPrizeArray;
    NSMutableArray*thirdPrizeArray;
    NSString *timeValue ;
   
    NSString*time ;
    long counterr ;
    
    NSMutableArray*ticketPriceArray;
    NSTimeInterval minuteDifference;
    
    NSTimer *dailyTimer;
    NSTimer *jackpotTimer ;
    NSTimer *hourlyTimer ;
    
    long dailyCounter;
      long hourlyCounter;
     long jackpotCounter;
    NSTimer   * lyTimer;
    
    
    NSMutableArray*drawTimingArray ;
    
    NSArray *hourlydata;
    NSArray *dailyData;
    NSArray *jackpotData;

    NSTimeInterval minuteDifferenceHourly;
    NSTimeInterval minuteDifferenceDaily;

    NSString*timeInter;
    NSString*credits;
    NSMutableArray*checkStatus ;
    
    id rechargeData ;
    AFHTTPSessionManager *manager ;
    
    
    NSString *checkUserType;
    
   
    NSMutableArray *bidValueArray;
//    UIImageView *imageForZooming;
    
    CGFloat timevAlue ;
    
    
    NSString *timeH;
     NSString *timeD;
     NSString *timeJ;

    NSString*checkTdJ ;
    
    NSString*type ;
    NSMutableArray*runningArray;
    
    NSString *apiUr;
    
    NSString*BidNumber;
    
    UIButton *btn;
    
    
    BOOL checkImageDownload ;
    
    NSArray *arrayImages ;
    
    UIImage *imgProfile ;
    
    BOOL isfirstIamges ;
    
    
    int countApi ;
    NSString*strPene ;
    
   NSMutableArray *winCountArray;
    NSMutableArray *lastPayoutArray;
    
    
}
@property (strong, nonatomic) UITextField *dropDownTxtfield;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;

@property (strong, nonatomic) IBOutlet UITableView *homeTableView;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSlide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
- (IBAction)menuClicked:(id)sender;


@property (strong, nonatomic) IBOutlet UITableView *sideTableView;
@property (strong, nonatomic) IBOutlet UIView *viewBanner;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControlMonthly;

@property (strong, nonatomic) IBOutlet UITextField *txtOperator;
@property (strong, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtpin1;
@property (strong, nonatomic) IBOutlet UITextField *txtPin2;
@property (strong, nonatomic) IBOutlet UITextField *txtPin3;
@property (strong, nonatomic) IBOutlet UITextField *txtPin4;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)submitClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtLottryType;
@property (strong, nonatomic) IBOutlet UITextField *txtBidAmt;
@property (strong, nonatomic) IBOutlet UIButton *btnBidSubmit;
- (IBAction)bidSubmitClicked:(id)sender;
- (IBAction)backBidClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *bidView;
@property (strong, nonatomic) IBOutlet UIView *bidSubView;
@property (strong, nonatomic) IBOutlet UITableView *detailTableView;

@property (strong, nonatomic) IBOutlet UILabel *txtSideName;

@property (strong, nonatomic) IBOutlet UIView *Viewimage;
@property (strong, nonatomic) IBOutlet UIImageView *SideImage;


@property (strong, nonatomic) IBOutlet UILabel *txtSideEmail;


@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomDate;

@property (strong, nonatomic) IBOutlet UITextField *txtStartDate;

@property (strong, nonatomic) IBOutlet UITextField *txtEndDate;

- (IBAction)dateDoneClicked:(id)sender;


- (IBAction)peneClicked:(id)sender;

-(IBAction)cancelBidClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *txtBoardNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblDraw;
@property (strong, nonatomic) IBOutlet UILabel *txtDrawTime;
@property (strong, nonatomic) IBOutlet UIButton *btnPene;
@property (strong, nonatomic) IBOutlet UILabel *txtTicketPrice;

@property (strong, nonatomic) IBOutlet UITableView *hourlyTableView;
@property (strong, nonatomic) IBOutlet UIImageView *hourlyImg;


////////////hourly //////////////

@property (strong, nonatomic) IBOutlet UIImageView *dailyImg;
@property (strong, nonatomic) IBOutlet UITableView *dailyTableView;

- (IBAction)peneDailyClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *txtDailyTime;
@property (strong, nonatomic) IBOutlet UILabel *lblDailyDraw;

@property (strong, nonatomic) IBOutlet UILabel *txtDailyBoard;

@property (strong, nonatomic) IBOutlet UIButton *btnPeneDaily;
@property (strong, nonatomic) IBOutlet UILabel *txtDailyticket;

///////monthly //////////
@property (strong, nonatomic) IBOutlet UITableView *monthlyTableView;
@property (strong, nonatomic) IBOutlet UIImageView *monthlyImg;
@property (strong, nonatomic) IBOutlet UILabel *txtMonthlyticket;
@property (strong, nonatomic) IBOutlet UILabel *txtMonthlyTime;

@property (strong, nonatomic) IBOutlet UILabel *txtMonthlyBoard;
@property (strong, nonatomic) IBOutlet UILabel *lblmonthlyDraw;

- (IBAction)peneMonthlyClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnPeneMonthly;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewHome;

@property (strong, nonatomic) IBOutlet UILabel *lblNoBlnc;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bannerHeight;


@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageContrl;



@property (strong, nonatomic) IBOutlet UITextField *txtRechAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnRecharge;
- (IBAction)rechargeClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewReharge;
- (IBAction)backRecharge:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtAgentNumber;

@property (strong, nonatomic) IBOutlet UILabel *LBLPENE;
@end
