//
//  customHomeCell.h
//  Pene
//
//  Created by katoch on 06/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customHomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtRank;
@property (strong, nonatomic) IBOutlet UILabel *txtPrize;
@property (strong, nonatomic) IBOutlet UILabel *txtRankName;

@end
