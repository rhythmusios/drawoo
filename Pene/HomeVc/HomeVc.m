//
//  HomeVc.m
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "HomeVc.h"

#import "AppDelegate.h"
#import "loginVc.h"
#import "sideMenuCell.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"
#import "UIImageView+WebCache.h"
#import "DropDownView.h"
#import "lottryProfileVc.h"
#import "customHomeCell.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomCell.h"
#import "HistoryVc.h"
#import "Reachability.h"


@interface HomeVc ()
@end

@implementation HomeVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   
    UITapGestureRecognizer *gesRecogniz1= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRechargeGesture)]; // Declare the Gesture.
    gesRecogniz1.delegate = self;
    [self.viewReharge addGestureRecognizer:gesRecogniz1];
    
    _btnRecharge.layer.cornerRadius = 15.0f ;
    _btnRecharge.layer.masksToBounds = YES;

    
    
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellWithReuseIdentifier:@"CustomCell"];
    
     userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    mobile = [[NSUserDefaults standardUserDefaults]valueForKey:@"mobile"];
    
    
    
    prizeAarray = [[NSMutableArray alloc]initWithObjects:@"Ist prize when all 4 number matches",@"2nd prize when all 3 number matches",@"3rd prize when all 2 number matches", nil];
    
    hourlyAmntArray = [[NSMutableArray alloc]init];
    dailyAmntArray = [[NSMutableArray alloc]init];
     monthlyAmntArray = [[NSMutableArray alloc]init];
    
    
    _SideImage.layer.cornerRadius = self.SideImage.frame.size.width/2 ;
    _SideImage.layer.masksToBounds = YES;
    
    _Viewimage.layer.cornerRadius = self.Viewimage.frame.size.width/2 ;
    _Viewimage.layer.masksToBounds = YES;
    
    
    
    _scrollViewHome.delegate = self;
    
//    _pageControl.numberOfPages = 3 ;
//    _pageControlMonthly.numberOfPages = 3;
    
    self.btnPene.layer.cornerRadius = 5.0 ;
    self.btnPene.layer.masksToBounds = YES;
    
    self.btnPeneDaily.layer.cornerRadius = 5.0 ;
    self.btnPeneDaily.layer.masksToBounds = YES;

    self.btnPeneMonthly.layer.cornerRadius = 5.0 ;
    self.btnPeneMonthly.layer.masksToBounds = YES;
   
    
  
    
//   [self getData];
    
    self.detailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    slidingArray = [[NSMutableArray alloc]initWithObjects:@"balls.jpg",@"dollers.jpeg",@"lottry.jpeg",@"Lucky_Cherries_Slider.jpg", nil];
    
    UIColor *color = [UIColor colorWithRed:250/255.0f green:159/255.0f blue:163/255.0f alpha:1];
    
//    _txtpin1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●" attributes:@{NSForegroundColorAttributeName: color}];
//    _txtPin2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●" attributes:@{NSForegroundColorAttributeName: color}];
//     _txtPin3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●" attributes:@{NSForegroundColorAttributeName: color}];
//    _txtPin4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●" attributes:@{NSForegroundColorAttributeName: color}];
    
    dropDownView.delegate = self ;
    
    
    UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [dropImage setImage:[UIImage imageNamed:@"dropDown.png"]];
    [dropDownPadding addSubview:dropImage];
    _txtOperator.rightViewMode = UITextFieldViewModeAlways;
    _txtOperator.rightView = dropDownPadding ;
    
    
    textStr = [[NSMutableString alloc]init];
//    operatorArray = [[NSMutableArray alloc]initWithObjects:@"Airtel",@"Vodafone",@"Idea",@"BSNL", nil];
     operatorArray = [[NSMutableArray alloc]init];
     lotteryArray = [[NSMutableArray alloc]initWithObjects:@"lottery1",@"lottery2",@"lottery3",@"lottery4", nil];
    
    self.bidSubView.layer.cornerRadius = 5.0f ;
    self.bidSubView.layer.masksToBounds = YES;
    
   self.btnSubmit.layer.cornerRadius = _btnSubmit.frame.size.height/2 ;
        self.btnSubmit.layer.masksToBounds = YES;
    
     self.btnBidSubmit.layer.cornerRadius = _btnBidSubmit.frame.size.height/2 ;
    self.btnBidSubmit.layer.masksToBounds = YES;
    
    //merchantTitleArray = [NSMutableArray alloc]
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width/2 ;
_imgProfile.layer.masksToBounds = YES;
    
  
   
    
    [_sideTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"pink"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    _viewMenuSlide.hidden = true;
   menuItemsSection1 = [[NSMutableArray alloc]initWithObjects:@"Profile",@"About us",@"Contact us",@"History",@"Privacy Policy",@"Terms & Conditions",@"Logout",nil];
    
    sectionImages1 = [[NSMutableArray alloc]initWithObjects:@"avatar",@"aboutUs",@"contactUs",@"history-symbol-of-antique-building (3)",@"privacy-60",@"terms",@"aboutUs",@"logout-60", nil];
    
    
   
    
    
    
//    [self.view bringSubviewToFront:_homeTableView];
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    
    UITapGestureRecognizer *gesRecogniz = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBidGesture)]; // Declare the Gesture.
    gesRecogniz.delegate = self;
    [self.bidView addGestureRecognizer:gesRecogniz];
    
   
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
    apiUr=@"" ;
    
    [hourlyTimer invalidate];
     manager = nil ;
    
    
    
//   [ manager.operationQueue cancelAllOperations];
//    manager.requestSerializer.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
//
//
//     [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
   
    countApi = 0 ;
    
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirstDownloading"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    checkImageDownload = true ;
    
    
     apiUr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/drw/live"];
    
      [self.viewReharge setHidden:YES];
    
     [self profileApiFromServer];
   
    ticketPriceArray = [[NSMutableArray alloc]initWithObjects:@"TZS 1,000",@"TZS 5,000",@"TZS 5,000", nil];
    
    

    [self.bidView setHidden:YES];
    
   
    
    [self.bidView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.7]];
    
    
    
    
    _viewMenuSlide.hidden = true;
    [self hideSlideMenu];
   
    [KVNProgress show];
    [self  SendDataOnServer];
    
    
}


-(void)hideSlideMenu{
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        _viewMenuSlide.hidden = true;
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_detailTableView]) {
        
        return NO;
    }
    else if ([touch.view isDescendantOfView:_sideTableView]) {
       
        return NO;
        
    }
    else if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }
    
    
    return YES;
}

-(void)tapBidGesture{
    
    [self.bidView setHidden:YES];
    [self.view endEditing:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
        return 1;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView== _sideTableView) {
        
        return menuItemsSection1.count ;
        
    }else if (tableView== _hourlyTableView) {
        
        return hourlyAmntArray.count ;
        
    }
    else if (tableView== _dailyTableView) {
        
        return hourlyAmntArray.count ;
        
    }
    else if (tableView== _monthlyTableView) {
        
        return hourlyAmntArray.count ;
        
    }
    else
    {

        return 0 ;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _sideTableView) {

        
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            return 45;
        }else{
            return 65 ;
            
        }
            

    }else if (tableView == _hourlyTableView) {
        
        return 48 ;
        
        
    }else if (tableView == _dailyTableView) {
        
        return 48 ;
        
        
    }else if (tableView == _monthlyTableView) {
        
        return 48 ;
        
        
    }
    
    
    else{
        
        return 140 ;
    }
    
    
}
    


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _hourlyTableView) {
        customHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customHomeCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"customHomeCell" owner:self options:nil];

            cell= arr[0];

        }
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.txtRankName.text = [prizeAarray objectAtIndex:indexPath.row];
        cell.txtPrize.text = [hourlyAmntArray objectAtIndex:indexPath.row];
        

        return cell;

    }else if (tableView == _dailyTableView) {
        customHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customHomeCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"customHomeCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.txtRankName.text = [prizeAarray objectAtIndex:indexPath.row];
        cell.txtPrize.text = [dailyAmntArray objectAtIndex:indexPath.row];
        
        
        
        return cell;
        
    }else if (tableView == _monthlyTableView) {
        customHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customHomeCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"customHomeCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.txtRankName.text = [prizeAarray objectAtIndex:indexPath.row];
        cell.txtPrize.text = [monthlyAmntArray objectAtIndex:indexPath.row];
        
        
        
        return cell;
        
    }
    else if (tableView== _sideTableView){
        sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
            
            cell= arr[0];
            UIView * views = [[UIView alloc]initWithFrame:cell.bounds];
            views.backgroundColor = [UIColor colorWithRed:0.80 green:0.73 blue:0.80 alpha:1.0];
            cell.selectedBackgroundView = views;
        }
        
        
         cell.txtMenu.text  = [menuItemsSection1 objectAtIndex:indexPath.row];
         cell.menuImg.image = [UIImage imageNamed:[sectionImages1 objectAtIndex:indexPath.row]];
        
        
//        cell.textLabel.font = [UIFont systemFontOfSize:12];
        
        return cell;
        
        
     }else{
          return 0;
     }
   
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_sideTableView) {

        if ([menuItemsSection1[indexPath.row] isEqualToString:@"Profile"]) {
            lottryProfileVc*tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"lottryProfileVc"];
            
      
            
            [self showViewController:tbc sender:self];
            
            
        }
//        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"How to play"])
//        {
//            PlayVc *play = [[PlayVc alloc]init];
//            play = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVc"];
//
//
//
//  [self showViewController:play sender:self];
//
//
//        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"About us"])
        {
            AboutVc *About = [[AboutVc alloc]init];
            About = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutVc"];
            
            [self showViewController:About sender:self];
        
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Contact us"])
        {
            ContactVc *contact = [[ContactVc alloc]init];
            contact = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactVc"];
            
            [self showViewController:contact sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"History"])
        {
            SideHistoryVc *his = [[SideHistoryVc alloc]init];
            his = [self.storyboard instantiateViewControllerWithIdentifier:@"SideHistoryVc"];
            his.checkStr = @"History";
            
            [self showViewController:his sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Privacy Policy"])
        {
            PrivcyVc *privac = [[PrivcyVc alloc]init];
            privac = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcyVc"];
            
            [self showViewController:privac sender:self];
            
        }else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Terms & Conditions"])
        {
            TermsVc *term = [[TermsVc alloc]init];
            term = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsVc"];
            
            [self showViewController:term sender:self];
            
        }
//        else if (indexPath.row == 7)
//        {
//            FaqVc *About = [[FaqVc alloc]init];
//            About = [self.storyboard instantiateViewControllerWithIdentifier:@"FaqVc"];
//
//            [self showViewController:About sender:self];
//
//        }
        
        
       else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Logout"]) {
                        UIAlertView*logout = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure you want to log out" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                        logout.tag = 11 ;
                        logout.delegate = self;
                        [logout show];
                                      }

    }


}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
     if (alertView.tag ==11){
        
        if (buttonIndex == 1) {
           
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            loginVc*lVc = [[loginVc alloc]init];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            lVc= [storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
            UINavigationController *navControllr = [[UINavigationController alloc]initWithRootViewController:lVc];
            navControllr.navigationBarHidden = true;
            delegate.window.rootViewController= navControllr;
            [delegate.window makeKeyAndVisible];
            
            
        }
    }else if (alertView.tag ==10){
        
        [self.bidView setHidden:YES];
        
    }
    
}


- (IBAction)menuClicked:(id)sender {
    
//     [self.view sendSubviewToBack:_homeTableView];
    
    
    [self.view bringSubviewToFront:self.viewMenuSlide];
    
   
    
     _viewMenuSlide.hidden = false;
        
        [UIView animateWithDuration:0.5 animations:^{
            _viewX.constant = 0;
            _viewWidth.constant = self.view.frame.size.width - 50 ;

            [self.view layoutIfNeeded];
            _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
            
        } completion:^(BOOL finished) {
//            _viewMenuSlide.hidden = true;
            
        }];
    
}

- (IBAction)backClicked:(id)sender {
    
    
}

-(void)tapGesture{
    
    [dropDownView closeAnimation];
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
                    _viewMenuSlide.hidden = true;
        
    }];
    
}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    
        
        return 0.0 ;
    
    
}



-(void)SendDataOnServer{
    
   
    //    10.10.1.57:8081
//    52.172.26.198:8081
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:nil message: @"No Network Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
        
        
    }else{

        
        
        if (![apiUr isEqualToString:@""]) {
            
          
            
            manager  = [AFHTTPSessionManager manager];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            
            
            
            [manager GET:apiUr parameters:nil success:^(NSURLSessionTask*task, id responseObject)
             
             {
                 NSLog(@"PLIST: %@", responseObject);
                 [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
                 
                 dicData = responseObject;
                 
                 [self performSelectorOnMainThread:@selector(getDataData) withObject:nil waitUntilDone:YES];
                 
                 
             }
                 failure:^(NSURLSessionTask *operation, NSError *error) {
                     [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
                     if (checkImageDownload == true) {
                         
                     }else{
                     
                     UIAlertController * alert = [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:@"Server Error"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                     UIAlertAction* noButton = [UIAlertAction
                                                actionWithTitle:@"Ok"
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    
                                                }];
                     
                     [alert addAction:noButton];
                     [self presentViewController:alert animated:YES completion:nil];
                     
                     }
                     
                 }];
            
        }
        }
   
    
    
    
}



-(void)getDataData{
    
    adsUrl = [[NSMutableArray alloc]init];
    
    if ([[dicData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        
        NSString*status = @"Running";
        NSPredicate *predicateRe = [NSPredicate
                                  predicateWithFormat:@"SELF.drwSts contains %@",
                                  status];
        
        
        
        
        NSArray*arrResult = [[dicData valueForKey:@"drawLst"] filteredArrayUsingPredicate:predicateRe];
        
        
        
        
        
        
        
        result = [[NSMutableArray alloc]init];
        
        
//        for (NSDictionary*dict in  [dicData valueForKey:@"drawLst"]) {
//
//                [result addObject:dict];
//
//        }
        
        
        
        
        
         NSString*hourly = @"HOURLY" ;
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.schdlTyp contains %@",
                                  hourly];
        hourlydata = [arrResult filteredArrayUsingPredicate:predicate];
        
        
        
        NSString*DAILY = @"DAILY" ;
        
        NSPredicate *predicateDaily = [NSPredicate
                                       predicateWithFormat:@"SELF.schdlTyp contains %@",
                                       DAILY];
        dailyData = [arrResult filteredArrayUsingPredicate:predicateDaily];
        
        
        
        NSString*JACKPOT = @"JACKPOT" ;
        NSPredicate *predJACKPOT = [NSPredicate
                                    predicateWithFormat:@"SELF.schdlTyp contains %@",
                                    JACKPOT];
        jackpotData = [arrResult filteredArrayUsingPredicate:predJACKPOT];
        
        
        if (hourlydata.count>0) {
            [result addObjectsFromArray:hourlydata];
        }
         if (dailyData.count>0) {
            [result addObjectsFromArray:dailyData];
        }
        
         if (jackpotData.count>0) {
            [result addObjectsFromArray:jackpotData];
        }
        
        
        
        if (checkImageDownload == true) {
            imagesUrlArray = [[NSMutableArray alloc]init];
        }
        
        
        drawTimeArray = [[NSMutableArray alloc]init];
        bidValueArray = [[NSMutableArray alloc]init];
        

        if ([result count] > 0) {
            
            NSMutableArray*arrImg = [[NSMutableArray alloc]init];
            
             if (checkImageDownload == true) {
            btnPeneArray = [[NSMutableArray alloc]init];
             }
                 
            istPrizeArray = [[NSMutableArray alloc]init];
             secondPrizeArray = [[NSMutableArray alloc]init];
             thirdPrizeArray = [[NSMutableArray alloc]init];
            lastPayoutArray = [[NSMutableArray alloc]init];
            winCountArray = [[NSMutableArray alloc]init];
            



            for (NSArray*arr in result) {

                arrImg = [arr valueForKey:@"tempImg"];
               NSString *strI = [arrImg objectAtIndex:0];
                
                 if (checkImageDownload == true) {
                [imagesUrlArray addObject:strI];
                 }
                
                if (checkImageDownload == true) {

                [btnPeneArray addObject:[arr valueForKey:@"schdlTyp"]];
                    
                }
                
                
                [winCountArray addObject:[arr valueForKey:@"wnCnt"]];
                 [lastPayoutArray addObject:[arr valueForKey:@"pytVlu"]];
                
                [istPrizeArray addObject:[arr valueForKey:@"przFctrOn"]];
                [secondPrizeArray addObject:[arr valueForKey:@"przFctrTw"]];
                [thirdPrizeArray addObject:[arr valueForKey:@"przFctrThr"]];
                [bidValueArray addObject:[arr valueForKey:@"bidAmt"]];
                
                NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[arr valueForKey:@"drawTym"]];
                NSInteger bidend = [BidEndStr integerValue];
                NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
                //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                NSString *BidEndDate = [formatter stringFromDate:endDate];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
                NSDate *drawDate = [dateFormatter dateFromString:BidEndDate];
                NSDate *currentDate = [NSDate date];
                
                NSString*type =  [NSString stringWithFormat:@"%@",[arr valueForKey:@"schdlTyp"]];
                
                if ([type isEqualToString:@"JACKPOT"]) {
                                //    CGFloat minuteDifference = [drawDate timeIntervalSinceDate:currentDate] / 60.0;
                    
                                NSCalendar *sysCalendar = [NSCalendar currentCalendar];
                                minuteDifference = [drawDate timeIntervalSinceDate:currentDate];
                    
                           jackpotCounter = minuteDifference ;
                    
//                    jackpotTimer = [NSTimer scheduledTimerWithTimeInterval: 0.5
//                                                             target: self
//                                                           selector: @selector(handleTimer:)
//                                                           userInfo: nil
//                                                            repeats: YES];
                    
                                // Create the NSDates
                                NSDate *date1 = [[NSDate alloc] init];
                                NSDate *date2 = [[NSDate alloc] initWithTimeInterval:minuteDifference sinceDate:date1];
                    
                                // Get conversion to months, days, hours, minutes
                                NSCalendarUnit unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSSecondCalendarUnit;
                    
                                NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
                                NSLog(@"Break down: %li min : %li hours : %li days : %li months", (long)[breakdownInfo minute], (long)[breakdownInfo hour], (long)[breakdownInfo day], (long)[breakdownInfo month]);
                    
                    
                    NSString* strt ;
                    if ([breakdownInfo day] == 0 && [breakdownInfo hour] == 0)  {
                        strt =  [NSString stringWithFormat:@"%i day %i hour",0,0];
                    }else{
                        
                        
                        strt =  [NSString stringWithFormat:@"%li days %li hour %li min %li sec",(long)[breakdownInfo day],(long) [breakdownInfo hour],(long) [breakdownInfo minute],(long) [breakdownInfo second]];
                    }
                    
    
                                [drawTimeArray addObject:strt];
                    
                    
                    
                    
                    }
                
                
               else if
                   
                   ([type isEqualToString:@"DAILY"]) {
                    
                   NSCalendar *sysCalendar = [NSCalendar currentCalendar];
                  minuteDifferenceDaily = [drawDate timeIntervalSinceDate:currentDate];
                    dailyCounter = minuteDifferenceDaily ;

                       
                   
                   // Create the NSDates
                   NSDate *date1 = [[NSDate alloc] init];
                   NSDate *date2 = [[NSDate alloc] initWithTimeInterval:minuteDifferenceDaily sinceDate:date1];
                   
                   // Get conversion to months, days, hours, minutes
                   NSCalendarUnit unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSSecondCalendarUnit;
                   
                   NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
                   NSLog(@"Break down: %li min : %li hours : %li days : %li months", (long)[breakdownInfo minute], (long)[breakdownInfo hour], (long)[breakdownInfo day], (long)[breakdownInfo month]);
                   
                   
                   NSString* strt ;
                   if ([breakdownInfo hour] == 0 && [breakdownInfo minute] == 0)  {
                       strt =  [NSString stringWithFormat:@"%i hour %i min",0,0];
                   }else{
                       
                       
                       strt =  [NSString stringWithFormat:@"%li hour %li min %li sec",(long)[breakdownInfo hour],(long) [breakdownInfo minute] ,(long) [breakdownInfo second]];
                   }
                   

                   
                   [drawTimeArray addObject:strt];
                   
//                       timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hourlyData) userInfo:nil repeats:YES];
                       
                }
               else if ([type isEqualToString:@"HOURLY"]) {
                   
                   NSCalendar *sysCalendar = [NSCalendar currentCalendar];
                   minuteDifferenceHourly = [drawDate timeIntervalSinceDate:currentDate];
                    hourlyCounter = minuteDifferenceHourly ;
                   
//                   hourlyTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
//                                                                   target: self
//                                                                 selector: @selector(hourlyData)
//                                                                 userInfo: nil
//                                                                  repeats: YES];

                   
                   // Create the NSDates
                   NSDate *date1 = [[NSDate alloc] init];
                   NSDate *date2 = [[NSDate alloc] initWithTimeInterval:minuteDifferenceHourly sinceDate:date1];
                   
                   // Get conversion to months, days, hours, minutes
                   NSCalendarUnit unitFlags = NSSecondCalendarUnit| NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                   
                   NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
                   NSLog(@"Break down: %li min : %li hours : %li days : %li months", (long)[breakdownInfo minute], (long)[breakdownInfo hour], (long)[breakdownInfo day], (long)[breakdownInfo month]);
                   
                   
                   
                   NSString* strt ;
                   if ([breakdownInfo minute] <= 0 && [breakdownInfo second] <= 0)  {
                       strt =  [NSString stringWithFormat:@"%i min %i sec",0,0];
                   }else{
                   
                   
                    strt =  [NSString stringWithFormat:@"%li min %li sec",(long)[breakdownInfo minute],(long) [breakdownInfo second]];
                   }
                   
                   
                   [drawTimeArray addObject:strt];
                   
               }
                
              
            }
            
            NSLog(@"%@",drawTimeArray);
            
            
            _pageContrl.numberOfPages = result.count ;

             checkImageDownload = false;
            
             countApi = countApi + 1 ;
//           [self.collectionView reloadData];
            
            
            
            
            [self.collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
           

            
            [self performSelectorOnMainThread:@selector(SendDataOnServer) withObject:nil waitUntilDone:YES];
            
           
            
        
    }
     else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"No data found"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }

    
}

}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    [self performSelectorOnMainThread:@selector(SendDataOnServer) withObject:nil waitUntilDone:YES];
}

-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}



-(void)operatorDropDown{
    
    dropDownView = [[DropDownView alloc] initWithArrayData:operatorArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:_txtOperator animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
    
    
    
    dropDownView.delegate = self;
    dropDownView.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:dropDownView.view];
    
    [self.view bringSubviewToFront:dropDownView.view];
    
    _dropDownTxtfield = _txtOperator ;
    
    [ dropDownView openAnimation];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    
    if (textField == self.txtpin1 || textField == self.txtPin2 || textField == self.txtPin3 || textField == self.txtPin4) {
       

        
        if ((textField.text.length >= 1) && (string.length > 0))
        {
            [textField setText:string];
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSLog(@"newtexr :%@",newText);
            
            [textStr replaceCharactersInRange:range withString:string];
            NSInteger nextTag = textField.tag + 1;
            // Try to find next responder
            UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
            if (! nextResponder)
                nextResponder = [textField.superview viewWithTag:1];
            
            if (nextResponder)
                if (textField.text.length == 1)
                {
                    [nextResponder becomeFirstResponder];
                    
                }
            // Found next responder, so set it.
            
            return NO;
        }
        else{
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if (newString.length == 0)
            {
                [textField setText:newString];
                
                NSString *newText = [textStr stringByReplacingCharactersInRange:range withString:string];
                NSLog(@"newtexr :%@",newText);
                [textStr replaceCharactersInRange:range withString:string];
                NSInteger nextTag = textField.tag - 1;
                // Try to find next responder
                UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
                if (! nextResponder)
                    nextResponder = [textField.superview viewWithTag:1];
                
                if (nextResponder)
                    [nextResponder becomeFirstResponder];
                
            }
            else
            {
                [textField setText:newString];
                [textStr appendString:newString];
                
                NSInteger nextTag = textField.tag + 1;
                // Try to find next responder
                UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
                if (! nextResponder)
                    nextResponder = [textField.superview viewWithTag:1];
                
                if (nextResponder)
                    if (textField.text.length == 1)
                    {
                        [nextResponder becomeFirstResponder];
                        
                    }
                
            }
            
            
            NSLog(@"string :%@",textStr);
            
            return NO;
        }
        
    }
    
    return YES;
}




-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomDate.constant = -270;
        [self.view layoutSubviews];
        
    }];
    
    
    if (textField == _txtOperator) {
        [dropDownView.view removeFromSuperview];
         [dropDownView closeAnimation];
        [self.view endEditing:YES];
        
        if (operatorArray.count >0) {
            
            [self   operatorDropDown];
            
        }else{
       
        }
        
        return NO ;
        
        
    }else  if (textField == _txtLottryType) {
        [dropDownView.view removeFromSuperview];
        
        [dropDownView closeAnimation];
        
        [self.view endEditing:YES];
        
        
        dropDownView = [[DropDownView alloc] initWithArrayData:lotteryArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
        
        
        
        dropDownView.delegate = self;
        dropDownView.view.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:dropDownView.view];
        
        [self.view bringSubviewToFront:dropDownView.view];
        
        _dropDownTxtfield = _txtLottryType ;
        
        [ dropDownView openAnimation];
        
        return NO ;
        
        
    }
    else if (textField == _txtStartDate) {
        
        
        checkDateStr = @"Start";
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }else if (textField == _txtEndDate) {
         checkDateStr = @"End";
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }
    
    return true;
}

-(void)dropDownCellSelected:(NSInteger)returnIndex{
    
    if (_dropDownTxtfield == _txtOperator) {
        
         _txtOperator.text = [operatorArray objectAtIndex:returnIndex];
    }
    else if (_dropDownTxtfield == _txtLottryType) {
        
         _txtLottryType.text = [lotteryArray objectAtIndex:returnIndex];
        
    }
    
}


- (IBAction)bidSubmitClicked:(id)sender {
    
    
    if ([_btnBidSubmit.titleLabel.text isEqualToString:@"Buy"]) {
        
        
        
        
        [self.viewReharge setHidden:NO];
        
//        NSString *phNo = @"*121#";
//                NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
//
//                if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
//                    [[UIApplication sharedApplication] openURL:phoneUrl];
//                } else
//                {
//                    UIAlertView*  calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//                    [calert show];
//                }
        
    }else{
    
     if ([_txtpin1.text isEqualToString:@""]||[_txtPin2.text isEqualToString:@""]||[_txtPin3.text isEqualToString:@""]||[_txtPin4.text isEqualToString:@""]) {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:@"Enter your 4-digit bid number"
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
//        else if ([_txtpin1.text isEqualToString:_txtPin2.text] || [_txtpin1.text isEqualToString:_txtPin3.text]||[_txtpin1.text isEqualToString:_txtPin4.text] || [_txtPin2.text isEqualToString:_txtPin3.text]|| [_txtPin2.text isEqualToString:_txtPin4.text] || [_txtPin3.text isEqualToString:_txtPin4.text] ){
//            
//            UIAlertController * alert = [UIAlertController
//                                         alertControllerWithTitle:nil
//                                         message:@"Duplicate numbers are not allowed"
//                                         preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction* noButton = [UIAlertAction
//                                       actionWithTitle:@"Ok"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action) {
//                                           
//                                       }];
//            
//            [alert addAction:noButton];
//            [self presentViewController:alert animated:YES completion:nil];
//            
//        }
        
        else{
            
            
            [KVNProgress show];
            
            [self bidPostToServer];
            
        }
        
    }
        
   
    
    
}


- (IBAction)backBidClicked:(id)sender {
    
    [self.bidView setHidden:YES];
    
}



-(void)bidPostToServer{
    
    
    
    BidNumber = [NSString stringWithFormat:@"%@%@%@%@",_txtpin1.text,_txtPin2.text,_txtPin3.text,_txtPin4.text];
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/drw/aplybid"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    //    NSString*consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"CosumerId"];
    
  
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    _params = @{
                
                @"usrId" :[NSString stringWithFormat:@"%@",userid],
                @"tmpltId" :[NSString stringWithFormat:@"%@",tempId],
                @"lvTmpltId" :[NSString stringWithFormat:@"%@",livTempId],
                @"bidAmt" :[NSString stringWithFormat:@"%@",bidAmountStr],
                @"mobile" : [NSString stringWithFormat:@"%@",mobile],
                @"bidNo" :[NSString stringWithFormat:@"%@",BidNumber]
                
                };
    
    
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         bidData = responseObject;
         
         [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
         
         
     }
           failure:^(NSURLSessionTask *operation, NSError *error) {
               [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
               UIAlertController * alert = [UIAlertController
                                            alertControllerWithTitle:nil
                                            message:@"Server Error"
                                            preferredStyle:UIAlertControllerStyleAlert];
               UIAlertAction* noButton = [UIAlertAction
                                          actionWithTitle:@"Ok"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              
                                          }];
               
               [alert addAction:noButton];
               [self presentViewController:alert animated:YES completion:nil];
               
               
               
           }];
    
    
    
}



-(void)checkResponse{
    
    
    if ([[bidData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        
        [self profileApiFromServer];
        
        
        
        
//        NSString *phNo = @"+918556841629";
//        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
//
//        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
//            [[UIApplication sharedApplication] openURL:phoneUrl];
//        } else
//        {
//            UIAlertView*  calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//            [calert show];
//        }
        
                UIAlertView*bid = [[UIAlertView alloc]initWithTitle:nil message:@"Bid has been successfully submitted" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                bid.tag = 10 ;
                bid.delegate = self;
                [bid show];
        
        
        
        
        
    }else{
        
       
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[bidData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



- (IBAction)submitClicked:(id)sender {
//    settingsVc *   lvc = [[settingsVc alloc]init];
//     lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsVc"];
//
//    [self.navigationController pushViewController:lvc animated:YES];

//    [self.view bringSubviewToFront:self.bidView];
//    [self.bidView setHidden:NO];
}


-(void)ShowSelectedDate{
    
    
    
    NSDateFormatter *Df = [[NSDateFormatter alloc]init];
    [Df setDateFormat:@"yyyy-MM-dd"];
    NSString*dateString =[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicker.date]];
    
    if ([checkDateStr isEqualToString: @"Start"]) {
        _txtStartDate.text = dateString ;
    }else{
        
        _txtEndDate.text = dateString ;
    }
    
    NSDate *dateFromString = [[NSDate alloc] init];
    Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFromString = [Df dateFromString:dateString];
    
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
    if ([checkDateStr isEqualToString: @"Start"]) {
        dtvalue = [dtTime integerValue];
    }else{
        
       endvalue = [dtTime integerValue];
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomDate.constant = -270;
        [self.view layoutSubviews];
        
    }];
}




- (IBAction)dateDoneClicked:(id)sender{
    
    
    [self ShowSelectedDate];
    
}

- (IBAction)peneClicked:(id)sender {
    

    
    if ([checkUserType isEqualToString:@"Individual"]) {
        
        _txtAgentNumber.hidden= YES;
        
    }
    else{
        
    _txtAgentNumber.hidden= NO;
        
    }
    
    

   btn = (UIButton*)sender ;
    NSLog(@"%@",btn.titleLabel.text);

    NSString*hourly = @"HOURLY" ;
    
    NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:@"SELF.schdlTyp contains %@",
                              hourly];
    
    NSArray*  hourlydata = [result filteredArrayUsingPredicate:predicate];
    
    
    
    NSString*DAILY = @"DAILY" ;
    
    NSPredicate *predicateDaily = [NSPredicate
                                   predicateWithFormat:@"SELF.schdlTyp contains %@",
                                   DAILY];
    
    NSArray* dailyData = [result filteredArrayUsingPredicate:predicateDaily];
    
    
    
    NSString*JACKPOT = @"JACKPOT" ;
    NSPredicate *predJACKPOT = [NSPredicate
                                predicateWithFormat:@"SELF.schdlTyp contains %@",
                                JACKPOT];
    
    NSArray*   jackpotData = [result filteredArrayUsingPredicate:predJACKPOT];
    
    
    
    
    if ([btn.titleLabel.text isEqualToString:@"Pene Hourly"]) {
        
        tempId = [NSString stringWithFormat:@"%@",[hourlydata[0] valueForKey:@"tmpltId"]];
        livTempId = [NSString stringWithFormat:@"%@",[hourlydata[0] valueForKey:@"lvTmpltId"]];
        bidAmountStr = [NSString stringWithFormat:@"%@",[hourlydata[0] valueForKey:@"bidAmt"]];
        
        
        
        NSInteger  cred = [credits integerValue];
        
        if (cred < 1000) {
             _lblNoBlnc.hidden = NO ;
             [_btnBidSubmit setTitle:@"Buy" forState:UIControlStateNormal];
            
        }else{
            
            [_btnBidSubmit setTitle:@"Submit" forState:UIControlStateNormal];
                    _lblNoBlnc.hidden = YES ;
            
            
        }
        
        

        
    }else if ([btn.titleLabel.text isEqualToString:@"Pene Daily"]){
        
        
        tempId = [NSString stringWithFormat:@"%@",[dailyData[0] valueForKey:@"tmpltId"]];
        livTempId = [NSString stringWithFormat:@"%@",[dailyData[0] valueForKey:@"lvTmpltId"]];
        bidAmountStr = [NSString stringWithFormat:@"%@",[dailyData[0] valueForKey:@"bidAmt"]];
        
        NSInteger  cred = [credits integerValue];
        
        if (cred < 5000) {
            _lblNoBlnc.hidden = NO ;
            [_btnBidSubmit setTitle:@"Buy" forState:UIControlStateNormal];
            
        }else{
            
            [_btnBidSubmit setTitle:@"Submit" forState:UIControlStateNormal];
            _lblNoBlnc.hidden = YES ;
            
            
        }

    }
    
    else if ([btn.titleLabel.text isEqualToString:@"Jackpot"]){
        
        tempId = [NSString stringWithFormat:@"%@",[jackpotData[0] valueForKey:@"tmpltId"]];
        livTempId = [NSString stringWithFormat:@"%@",[jackpotData[0]  valueForKey:@"lvTmpltId"]];
        bidAmountStr = [NSString stringWithFormat:@"%@",[jackpotData[0] valueForKey:@"bidAmt"]];
        
         NSInteger  cred = [credits integerValue];
        if (cred < 5000) {
            _lblNoBlnc.hidden = NO ;
            [_btnBidSubmit setTitle:@"Buy" forState:UIControlStateNormal];
            
        }else{
            
            [_btnBidSubmit setTitle:@"Submit" forState:UIControlStateNormal];
            _lblNoBlnc.hidden = YES ;
            
            
        }
        
       
        
        
        
    }
    
    
   
   
    
    
    
//    int amount = 6000 ;
//
//    _txtTicketPrice.text= [NSString stringWithFormat:@"TZS %i",amount];
//
//    if (amount < 5000) {
//
//        [_btnBidSubmit setTitle:@"Buy" forState:UIControlStateNormal];
//         _lblNoBlnc.hidden = NO ;
//    }else{
//
//        [_btnBidSubmit setTitle:@"Submit" forState:UIControlStateNormal];
//         _lblNoBlnc.hidden = YES ;
//
//    }
    
    
    [self.txtpin1 becomeFirstResponder];
    
    self.txtPin2.text = @"";
    self.txtpin1.text = @"";
    self.txtPin3.text = @"";
    self.txtPin4.text = @"";
    
    
    [self.bidView setHidden:NO];

  
    
    
}

//-(void) sendEmailInBackground {
//    NSLog(@"Start Sending");
//    SKPSMTPMessage *emailMessage = [[SKPSMTPMessage alloc] init];
//    emailMessage.fromEmail = @"sahilcube@gmail.com"; //sender email address
//    emailMessage.toEmail = @"katochluckei@gmail.com";  //receiver email address
//    emailMessage.relayHost = @"smtp.gmail.com";
//    //emailMessage.ccEmail =@"your cc address";
//    //emailMessage.bccEmail =@"your bcc address";
//    emailMessage.requiresAuth = YES;
//    emailMessage.login = @"sahilcube@gmail.com"; //sender email address
//    emailMessage.pass = @"sahil8283"; //sender email password
//    emailMessage.subject = @"code";
//    emailMessage.wantsSecure = YES;
//    emailMessage.delegate = self; /// you must include <SKPSMTPMessageDelegate> to your class
//    NSString *messageBody = BidNumber;
//    //for example :   NSString *messageBody = [NSString stringWithFormat:@"Tour Name: %@\nName: %@\nEmail: %@\nContact No: %@\nAddress: %@\nNote: %@",selectedTour,nameField.text,emailField.text,foneField.text,addField.text,txtView.text];
//    // Now creating plain text email message
//    NSDictionary *plainMsg = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey, messageBody,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
//    emailMessage.parts = [NSArray arrayWithObjects:plainMsg,nil];
//    //in addition : Logic for attaching file with email message.
//    /*
//     NSString *filePath = [[NSBundle mainBundle] pathForResource:@"filename" ofType:@"JPG"];
//     NSData *fileData = [NSData dataWithContentsOfFile:filePath];
//     NSDictionary *fileMsg = [NSDictionary dictionaryWithObjectsAndKeys:@"text/directory;\r\n\tx- unix-mode=0644;\r\n\tname=\"filename.JPG\"",kSKPSMTPPartContentTypeKey,@"attachment;\r\n\tfilename=\"filename.JPG\"",kSKPSMTPPartContentDispositionKey,[fileData encodeBase64ForData],kSKPSMTPPartMessageKey,@"base64",kSKPSMTPPartContentTransferEncodingKey,nil];
//     emailMessage.parts = [NSArray arrayWithObjects:plainMsg,fileMsg,nil]; //including plain msg and attached file msg
//     */
//    [emailMessage send];
//    // sending email- will take little time to send so its better to use indicator with message showing sending...
//}


- (IBAction)cancelBidClicked:(id)sender {
    [self.bidView setHidden:YES];
}



-(void)profileApiFromServer{
    
    
  

    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/%@",userid];
    
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        
        
        profileData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(profileMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
    

-(void)profileMessage
{
    
    if ([[profileData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        [[SDImageCache sharedImageCache]clearMemory];
        [[SDImageCache sharedImageCache]clearDisk];
        
        NSString*str = [NSString stringWithFormat:@"%@ %@",[profileData valueForKey:@"firstNm"] ,[profileData valueForKey:@"lastNm"]];
        _txtSideName.text = str ;
        
        NSString* mobile = [profileData valueForKey:@"mobile"];
        
        _txtSideEmail.text = mobile ;
        
      
      credits = [NSString stringWithFormat:@"%@",[profileData valueForKey:@"credits"]];
        
        NSMutableArray *arr = [[NSMutableArray alloc]initWithObjects:@"hourly.jpg",@"daily.jpg",@"jackpot.jpg", nil];
        
          [self.SideImage sd_setImageWithURL:[NSURL URLWithString:[profileData valueForKey:@"prflUrl"]]placeholderImage:[UIImage imageNamed:@"no-photo.jpg"]];
        
        checkUserType = [profileData valueForKey:@"userType"];
        
        
        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"UserName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[profileData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    NSInteger pagenumber = _scrollViewHome.contentOffset.x / _scrollViewHome.bounds.size.width;
    
    
    if (pagenumber == 2) {
        _pageControl.currentPage = 3 ;
    }else if (pagenumber == 1) {
        _pageControl.currentPage = 1 ;
    }else if (pagenumber == 0) {
        _pageControl.currentPage = 0 ;
    }
    
  
    
    
}

- (IBAction)peneMonthlyClicked:(id)sender {
    
      tempId = [NSString stringWithFormat:@"%@",[[dicData valueForKey:@"livJackPot"]valueForKey:@"tmpltId"]];
     livTempId = [NSString stringWithFormat:@"%@",[[dicData valueForKey:@"livJackPot"]valueForKey:@"lvTmpltId"]];
     bidAmountStr = [NSString stringWithFormat:@"%@",[[dicData valueForKey:@"livJackPot"]valueForKey:@"bidAmt"]];
    
    
    int amount = 4000 ;
    
      _txtMonthlyticket.text= [NSString stringWithFormat:@"TZS %i",amount];
    
    if (amount < 5000) {
        
          [_btnBidSubmit setTitle:@"Buy" forState:UIControlStateNormal];
        _lblNoBlnc.hidden = NO ;
    }else{
        
        [_btnBidSubmit setTitle:@"Submit" forState:UIControlStateNormal];
         _lblNoBlnc.hidden = YES ;
        
    }
    
      [self.bidView setHidden:NO];
}
    
    
- (IBAction)peneDailyClicked:(id)sender {
    
    tempId = [NSString stringWithFormat:@"%@",[[dicData valueForKey:@"livDaily"]valueForKey:@"tmpltId"]];
    livTempId = [NSString stringWithFormat:@"%@",[[dicData valueForKey:@"livDaily"]valueForKey:@"lvTmpltId"]];
      bidAmountStr = [NSString stringWithFormat:@"%@",[[dicData valueForKey:@"livDaily"]valueForKey:@"bidAmt"]];
    
    
    int amount = 6000 ;
    
      _txtDailyticket.text= [NSString stringWithFormat:@"TZS %i",amount];
    if (amount < 5000) {
        
        [_btnBidSubmit setTitle:@"Buy" forState:UIControlStateNormal];
         _lblNoBlnc.hidden = NO ;
    }else{
        
        [_btnBidSubmit setTitle:@"Submit" forState:UIControlStateNormal];
         _lblNoBlnc.hidden = YES ;
        
    }
    
      [self.bidView setHidden:NO];
}


////////////////////////////////// collectionView ////////////////////////

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return result.count ;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"CustomCell";
    CustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([UIScreen mainScreen].bounds.size.width == 320 ) {
        cell.heightImg.constant = 150 ;
        
    }else if ([UIScreen mainScreen].bounds.size.width == 375 ) {
        cell.heightImg.constant = 170 ;
        
    }else if ([UIScreen mainScreen].bounds.size.width == 414) {
        cell.heightImg.constant = 190 ;
        
    }
    else if ([UIScreen mainScreen].bounds.size.width == 768) {
        cell.heightImg.constant = 350 ;
        
    }
    else if ([UIScreen mainScreen].bounds.size.width == 1024) {
        cell.heightImg.constant = 400 ;
        
    }
    
    
    
    cell.btnPene.layer.cornerRadius = 15.0f ;
    cell.btnPene.layer.masksToBounds = YES ;
 
   
        
        [cell.imgBanner sd_setImageWithURL:[NSURL URLWithString:[imagesUrlArray objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"no-image.jpg"]];
    
  
    
    strPene = [NSString stringWithFormat:@"Pene %@",[btnPeneArray objectAtIndex:indexPath.row]];
    
    
        if ([strPene isEqualToString:@"Pene DAILY"]) {
            [cell.btnPene setTitle:@"Pene Daily" forState:UIControlStateNormal];
            
        }else if ([strPene isEqualToString:@"Pene HOURLY"]) {
            
            [cell.btnPene setTitle:@"Pene Hourly" forState:UIControlStateNormal];
            
        }else if ([strPene isEqualToString:@"Pene JACKPOT"]) {
            
            [cell.btnPene setTitle:@"Jackpot" forState:UIControlStateNormal];
        }
    
    
    cell.lastPayout.text = [NSString stringWithFormat:@"%@",[lastPayoutArray objectAtIndex:indexPath.row]];
     cell.winnersCount.text = [NSString stringWithFormat:@"%@",[winCountArray objectAtIndex:indexPath.row]];
    
    
    
//    if (checkImageDownload == true) {
//
//         [cell.btnPene setTitle:[NSString stringWithFormat:@"Pene %@",[btnPeneArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
//    }
   
    
    NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[result valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
    NSInteger bidend = [BidEndStr integerValue];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
//        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSString *BidEndDate = [formatter stringFromDate:endDate];
    
    NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:endDate];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
    NSDate *drawDate = [dateFormatter dateFromString:BidEndDate];
    NSDate *currentDate = [NSDate date];
    
//    CGFloat minuteDifference = [drawDate timeIntervalSinceDate:currentDate] / 60.0;
    
    NSTimeInterval minuteDifference = [drawDate timeIntervalSinceDate:currentDate];
    
    NSInteger ti = (NSInteger)minuteDifference ;

    NSInteger sec = ti % 60 ;
    NSInteger min = (ti /60)%60;

 timeValue = [NSString stringWithFormat:@"%.02f", minuteDifference];
    if (min < 0 && sec < 0 ) {
        timeValue = 0  ;
        cell.timeDraw.text = [NSString stringWithFormat:@"%d min %d sec",0,0];

    }else{

         cell.timeDraw.text =  [NSString stringWithFormat:@"%ld min %ld sec",(long)min,(long)sec];
    }

    
    
    
    cell.timeDraw.text = [drawTimeArray objectAtIndex:indexPath.row] ;

    
    if ([UIScreen mainScreen].bounds.size.width == 320) {
        
        [cell.lblthirdPriz setFont:[UIFont systemFontOfSize:10]];
         [cell.lblSecondPriz setFont:[UIFont systemFontOfSize:10]];
         [cell.lblFirstPriz setFont:[UIFont systemFontOfSize:10]];
        
        [cell.thirdPrize setFont:[UIFont systemFontOfSize:10]];
        [cell.istPrize setFont:[UIFont systemFontOfSize:10]];
        [cell.secondPrize setFont:[UIFont systemFontOfSize:10]];
        
        
    }else if ([UIScreen mainScreen].bounds.size.width == 375) {
        
        [cell.lblthirdPriz setFont:[UIFont systemFontOfSize:12]];
        [cell.lblSecondPriz setFont:[UIFont systemFontOfSize:12]];
        [cell.lblFirstPriz setFont:[UIFont systemFontOfSize:12]];
        
        [cell.thirdPrize setFont:[UIFont systemFontOfSize:12]];
        [cell.istPrize setFont:[UIFont systemFontOfSize:12]];
        [cell.secondPrize setFont:[UIFont systemFontOfSize:12]];
        
    }
    
    else  {
        
        [cell.lblthirdPriz setFont:[UIFont systemFontOfSize:12]];
        [cell.lblSecondPriz setFont:[UIFont systemFontOfSize:12]];
        [cell.lblFirstPriz setFont:[UIFont systemFontOfSize:12]];
        
        [cell.thirdPrize setFont:[UIFont systemFontOfSize:12]];
        [cell.istPrize setFont:[UIFont systemFontOfSize:12]];
        [cell.secondPrize setFont:[UIFont systemFontOfSize:12]];
    }
    
    
    if ([strPene isEqualToString:@"Pene HOURLY"]) {
        
//         if (checkImageDownload == true) {
//         [cell.btnPene setTitle:@"Pene Hourly" forState:UIControlStateNormal];
//
//         }
        
        cell.thirdPrize.text = [NSString stringWithFormat:@"TZS 1000 x %@",[thirdPrizeArray objectAtIndex:indexPath.row]];
        cell.secondPrize.text = [NSString stringWithFormat:@"TZS 1000 x %@",[secondPrizeArray objectAtIndex:indexPath.row]];
        cell.istPrize.text = [NSString stringWithFormat:@"TZS 1000 x %@",[istPrizeArray objectAtIndex:indexPath.row]];
        
        cell.thirdPrize.hidden = NO ;
        cell.istPrize.hidden= NO;
        cell.secondPrize.hidden = NO;
        
        
        cell.lblSecond.hidden = NO ;
        cell.lblThird.hidden= NO;
        
//        cell.txtTicketPrice.text = @"TZS 1000" ;

        cell.lblSec.hidden = NO;
        cell.lblThree.hidden = NO;
        cell.lblSecondPriz.hidden= NO;
        cell.lblthirdPriz.hidden= NO;
        
        
    }
    
    else if ([strPene isEqualToString:@"Pene DAILY"]) {
//         if (checkImageDownload == true) {
//         [cell.btnPene setTitle:@"Pene Daily" forState:UIControlStateNormal];
//         }
        
        cell.thirdPrize.text = [NSString stringWithFormat:@"TZS 5000 x %@",[thirdPrizeArray objectAtIndex:indexPath.row]];
        cell.secondPrize.text = [NSString stringWithFormat:@"TZS 5000 x %@",[secondPrizeArray objectAtIndex:indexPath.row]];
        cell.istPrize.text = [NSString stringWithFormat:@"TZS 5000 x %@",[istPrizeArray objectAtIndex:indexPath.row]];
        cell.thirdPrize.hidden = YES ;
        cell.istPrize.hidden= NO;
        cell.secondPrize.hidden = NO;
        
        cell.lblSecond.hidden = NO ;
        cell.lblThird.hidden= YES;
        cell.lblSec.hidden = NO;
        cell.lblThree.hidden = YES;
        cell.lblSecondPriz.hidden= NO;
        cell.lblthirdPriz.hidden= YES;

//         cell.txtTicketPrice.text = @"TZS 5000" ;
        
    }else if ([strPene isEqualToString:@"Pene JACKPOT"]) {
//         if (checkImageDownload == true) {
//        [cell.btnPene setTitle:@"Jackpot" forState:UIControlStateNormal];
//         }
        cell.thirdPrize.text = [NSString stringWithFormat:@"TZS 5000 x %@",[thirdPrizeArray objectAtIndex:indexPath.row]];
        cell.secondPrize.text = [NSString stringWithFormat:@"TZS 5000 x %@",[secondPrizeArray objectAtIndex:indexPath.row]];
        cell.istPrize.text = [NSString stringWithFormat:@"TZS 5000 x %@",[istPrizeArray objectAtIndex:indexPath.row]];
        
        
        cell.thirdPrize.hidden = YES ;
        cell.istPrize.hidden= NO;
        cell.secondPrize.hidden = YES;
        
        
        cell.lblSecond.hidden = YES ;
        cell.lblThird.hidden= YES;
        cell.lblSecondPriz.hidden= YES;
         cell.lblthirdPriz.hidden= YES;
        
        cell.lblSec.hidden = YES;
        cell.lblThree.hidden = YES;
        
        
        
//         cell.txtTicketPrice.text = @"TZS 5000" ;
        
        

    }
    
    
    cell.txtTicketPrice.text = [NSString stringWithFormat:@"TZS %@",[bidValueArray objectAtIndex:indexPath.row]];
    
    
    [cell.btnPene addTarget:self action:@selector(peneClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    
    
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);



}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    //    return UIEdgeInsetsMake(50, 20, 50, 20);
    return UIEdgeInsetsMake(0,0, 0, 0);
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _collectionView.frame.size.width;
    float currentPage = _collectionView.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        _pageContrl.currentPage = currentPage + 1;
    }
    else
    {
        _pageContrl.currentPage = currentPage;
    }
    
    
  
    
    NSLog(@"Page Number : %ld", (long)_pageContrl.currentPage);
    
}


-(void)hourlyData
{
    if (hourlyCounter==0)
        
        
    {
        
        [hourlyTimer invalidate];
        

        
    }
    
    
    else
        
        
    {
        hourlyCounter--;
        
        
        
        CustomCell *cell = (CustomCell*)[self.view viewWithTag:1];
        
      
        
        NSInteger ti = (NSInteger)hourlyCounter ;
        
        NSInteger sec = ti % 60 ;
        NSInteger min = (ti /60)%60;
        
        timeValue = [NSString stringWithFormat:@"%.02f", minuteDifferenceHourly];
        if (min < 0 && sec < 0 ) {
            timeValue = 0  ;
            time = [NSString stringWithFormat:@"%d min %d sec",0,0];
            
        }else{
            
            time =  [NSString stringWithFormat:@"%ld min %ld sec",(long)min,(long)sec];
        }
        
        
        if ([_btnPene.titleLabel.text isEqualToString:@"Pene Hourly"]) {
             cell.timeDraw.text = time ;
        }
        
        
        
//        [self.collectionView reloadData];

        
    }
}



-(void)dailyData
{
    if (dailyCounter==0)
        
        
    {
        
        [dailyTimer invalidate];
        
        
        
    }
    
    
    else
        
        
    {
        dailyCounter--;
        
        
        
        CustomCell *cell = (CustomCell*)[self.view viewWithTag:1];
        NSInteger ti = (NSInteger)dailyCounter ;
        
        NSInteger sec = ti % 60 ;
        NSInteger min = (ti /60)%60;
        
        timeValue = [NSString stringWithFormat:@"%.02f", minuteDifference];
        if (min < 0 && sec < 0 ) {
            timeValue = 0  ;
            time = [NSString stringWithFormat:@"%d min %d sec",0,0];
            
        }else{
            
            time =  [NSString stringWithFormat:@"%ld min %ld sec",(long)min,(long)sec];
        }
        
        
        if ([_btnPene.titleLabel.text isEqualToString:@"Pene Daily"]) {
            cell.timeDraw.text = time ;
        }
     
        
        //        [self.collectionView reloadData];
        
        
    }
}

-(void)jackpotData
{
    if (jackpotCounter==0)
        
        
    {
        
        [jackpotTimer invalidate];
        
        
        
    }
    
    
    else
        
        
    {
        jackpotCounter--;
        
        
        
        CustomCell *cell = (CustomCell*)[self.view viewWithTag:1];
        
        
        
        NSInteger ti = (NSInteger)jackpotCounter ;
        
        NSInteger sec = ti % 60 ;
        NSInteger min = (ti /60)%60;
        
        timeValue = [NSString stringWithFormat:@"%.02f", minuteDifference];
        if (min < 0 && sec < 0 ) {
            timeValue = 0  ;
            time = [NSString stringWithFormat:@"%d min %d sec",0,0];
            
        }else{
            
            time =  [NSString stringWithFormat:@"%ld min %ld sec",(long)min,(long)sec];
        }
        
        cell.timeDraw.text = time ;
        
        //        [self.collectionView reloadData];
        
       
    }
}

//////////////////////////// recharge //////////////////////


- (IBAction)rechargeClicked:(id)sender {
    
    [self.bidView setHidden:YES];
    
    if ([_txtRechAmount.text isEqualToString:@""]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your recharge amount"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        [KVNProgress show];
        [self  RechargeGetApiFromServer];
        
        
    }
}

-(void)RechargeGetApiFromServer{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/recharge?credits=%@&id=%@",_txtRechAmount.text ,userid];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         rechargeData = responseObject;
         
         [self performSelectorOnMainThread:@selector(rechargeData) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              UIAlertController * alert = [UIAlertController
                                           alertControllerWithTitle:nil
                                           message:@"Server Error"
                                           preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction* noButton = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             
                                         }];
              
              [alert addAction:noButton];
              [self presentViewController:alert animated:YES completion:nil];
              
              
              
          }];
    
    
    
}

-(void)rechargeData{
    
    
    
    
    if ([[rechargeData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
       
        [self profileApiFromServer];
        
        NSString *phNo = @"*121#";
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            UIAlertView*  calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [calert show];
        }
        
    
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[rechargeData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

-(void)tapRechargeGesture{
    [self.viewReharge setHidden:YES];
    
}

- (IBAction)backRecharge:(id)sender {
    [self.viewReharge setHidden:YES];
}
@end
