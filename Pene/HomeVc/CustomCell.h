//
//  CustomCell.h
//  Pene
//
//  Created by katoch on 07/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightImg;
@property (strong, nonatomic) IBOutlet UIImageView *imgBanner;
@property (strong, nonatomic) IBOutlet UIButton *btnPene;
@property (strong, nonatomic) IBOutlet UILabel *timeDraw;
@property (strong, nonatomic) IBOutlet UILabel *txtTicketPrice;
@property (strong, nonatomic) IBOutlet UILabel *istPrize;
@property (strong, nonatomic) IBOutlet UILabel *secondPrize;
@property (strong, nonatomic) IBOutlet UILabel *thirdPrize;
@property (strong, nonatomic) IBOutlet UILabel *lblSecond;
@property (strong, nonatomic) IBOutlet UILabel *lblThird;

@property (strong, nonatomic) IBOutlet UILabel *lblSec;
@property (strong, nonatomic) IBOutlet UILabel *lblThree;

@property (strong, nonatomic) IBOutlet UILabel *lblSecondPriz;
@property (strong, nonatomic) IBOutlet UILabel *lblthirdPriz;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstPriz;
@property (strong, nonatomic) IBOutlet UILabel *lastPayout;
@property (strong, nonatomic) IBOutlet UILabel *winnersCount;

@end
