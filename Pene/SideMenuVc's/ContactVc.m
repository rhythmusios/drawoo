//
//  ContactVc.m
//  Pene
//
//  Created by katoch on 07/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "ContactVc.h"

@interface ContactVc ()

{
    CGFloat size ;
    CGFloat TxtSize;
}

@end

@implementation ContactVc

- (void)viewDidLoad {
    [super viewDidLoad];
    size = self.view.bounds.size.width;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (size == 320 || size == 375 || size == 414)
    {
        TxtSize = 14;
        self.lblTechPVTLmt.font = [UIFont boldSystemFontOfSize:TxtSize - 4];

    }else if (size == 768 || size == 1024)
    {
        TxtSize = 25;
        
        self.lblTechPVTLmt.font = [UIFont boldSystemFontOfSize:TxtSize - 10];

    }else
    {
        TxtSize = 20;
        
        self.lblTechPVTLmt.font = [UIFont boldSystemFontOfSize:TxtSize - 6];

    }
    
    self.lblAddress.font = [UIFont boldSystemFontOfSize:TxtSize];
    self.lblContactNum.font = [UIFont boldSystemFontOfSize:TxtSize];
    self.lblEmail.font = [UIFont boldSystemFontOfSize:TxtSize];
    
    self.lblEmail.font = [UIFont boldSystemFontOfSize:TxtSize];
    
    
    
}

- (IBAction)backClicked:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

@end
