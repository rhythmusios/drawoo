//
//  UpdateVc.m
//  Swipeler
//
//  Created by katoch on 19/01/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "UpdateVc.h"
#import <PhotosUI/PhotosUI.h>

@interface UpdateVc ()

@end

@implementation UpdateVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _imgCamera.layer.cornerRadius = self.imgCamera.frame.size.width/2 ;
    _imgCamera.layer.masksToBounds = YES;
    
    _viewImage.layer.cornerRadius = self.viewImage.frame.size.width/2 ;
    _viewImage.layer.masksToBounds = YES;
    
    
    _otpView.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.6];
    
    self.otpView.layer.cornerRadius = 5.0f ;
    [_otpView clipsToBounds];
    
    
    self.btnVarify.layer.cornerRadius = _btnVarify.frame.size.height/2 ;
    
    cuCode = @"255";
    [self setAlertCtrl];
    
    
    //    operatorArray = [[NSMutableArray alloc]initWithObjects:@"Airtel",@"Vodafone",@"Jio", nil];
    
    [_lblBusinessDetailsTitle setHidden:NO];
    operatorArray = [[NSMutableArray alloc]init];
    usrArray = [[NSMutableArray alloc]initWithObjects:@"Individual",@"Agent", nil];
    
//    UIView *userTypePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
//    countryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
//    [countryImage setImage:[UIImage imageNamed:@"tanzania.png"]];
//    [userTypePadding addSubview:countryImage];
//    _txtCountry.leftViewMode = UITextFieldViewModeAlways;
//    _txtCountry.leftView = userTypePadding ;
    
    [_txtCountry setHidden:YES];
    
    
    [self.searchBar setReturnKeyType:NO];
    
    [self.searchBar setBackgroundImage:[[UIImage alloc]init]];
    [_searchBar setTintColor:[UIColor whiteColor]];
    
    [self.countryView setHidden:YES];
    
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    
    
    [self textfieldsPadding];
    self.btnNext.layer.cornerRadius = _btnNext.frame.size.height/2 ;
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [_otpView setHidden:YES];
    
    
    
    _txtGmail.text = _emailId ;
    _txtName.text = _firstName ;
    _txtLastName.text = _secondName ;
    _txtDOB.text = _dateOb;
    _userType.text = _type ;
    _txtMobile.text = _mobileNumber ;
       userId =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    
    
    //     _datePicker.maximumDate=[NSDate date];
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setYear: -12];
    NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    [comps setYear: -100];
    NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    
    
    self.datePicker.minimumDate = minDate;
    self.datePicker.maximumDate = maxDate;
    self.datePicker.date = maxDate;
    
    countryArray = [[NSMutableArray alloc]init];
//    [self CountryApiFromServer];
    
}

-(void)kvnStart{
    
    [KVNProgress show];
    
}

-(void)tapGesture{
    [dropDownView closeAnimation];
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        return NO;
    }else if ([touch.view isDescendantOfView:_countryTableView]) {
        return NO;
    }
    return YES;
    
}



- (IBAction)loginClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextClicked:(id)sender {
    
    if (![self emailStringIsValidEmail :self.txtGmail.text]){
        
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"Please enter the valid email address"
                                             preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
        
                                           }];
        
                [alert addAction:noButton];
                [self presentViewController:alert animated:YES completion:nil];
        
        
    }else{
    [self UpdateApiToServer];
    }
}





- (IBAction)backClicked:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}



-(void)textfieldsPadding{
    
    
    
    
    UIView *namePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *nameImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [nameImage setImage:[UIImage imageNamed:@"UserIcon"]];
    [namePadding addSubview:nameImage];
    _txtName.leftViewMode = UITextFieldViewModeAlways;
    _txtName.leftView = namePadding ;
    
    
    UIView *individualPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *individualImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [individualImage setImage:[UIImage imageNamed:@"UserIcon"]];
    [individualPadding addSubview:individualImage];
    _userType.leftViewMode = UITextFieldViewModeAlways;
    _userType.leftView = individualPadding ;
    
    
    UIView *operPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *operImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [operImage setImage:[UIImage imageNamed:@"UserIcon"]];
    [operPadding addSubview:operImage];
    _txtOperator.leftViewMode = UITextFieldViewModeAlways;
    _txtOperator.leftView = operPadding ;
    
    
    //    UIView *lastnamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    //    UIImageView *lastNameImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    //    [lastNameImage setImage:[UIImage imageNamed:@"name.png"]];
    //    [lastnamePadding addSubview:lastNameImage];
    //    .leftViewMode = UITextFieldViewModeAlways;
    //    _txtName.leftView = lastnamePadding ;
    
    
    
    UIView *mobilePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *mobileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [mobileImage setImage:[UIImage imageNamed:@"mobile-phone"]];
    [mobilePadding addSubview:mobileImage];
    _txtMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtMobile.leftView = mobilePadding ;
    
    UIView *emailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *emailImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [emailImage setImage:[UIImage imageNamed:@"envelope"]];
    [emailPadding addSubview:emailImage];
    _txtGmail.leftViewMode = UITextFieldViewModeAlways;
    _txtGmail.leftView = emailPadding ;
    
    UIView *aadharPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *aadharimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [aadharimage setImage:[UIImage imageNamed:@"small-calendar"]];
    [aadharPadding addSubview:aadharimage];
    _txtDOB.leftViewMode = UITextFieldViewModeAlways;
    _txtDOB.leftView = aadharPadding ;
    
    UIView *genderPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *firmImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [firmImage setImage:[UIImage imageNamed:@"firm.png"]];
    [genderPadding addSubview:firmImage];
    _txtGender.leftViewMode = UITextFieldViewModeAlways;
    _txtGender.leftView = genderPadding ;
    
    
   // UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    //UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    //[dropImage setImage:[UIImage imageNamed:@"dropDown.png"]];
    //[dropDownPadding addSubview:dropImage];
    //_txtOperator.rightViewMode = UITextFieldViewModeAlways;
    //_txtOperator.rightView = dropDownPadding ;
    
    
    
    
//    UIView *dropDownPadding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//    UIImageView *dropImage1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//    [dropImage1 setImage:[UIImage imageNamed:@"down-arrow"]];
//    [dropDownPadding1 addSubview:dropImage1];
//    _userType.rightViewMode = UITextFieldViewModeAlways;
//    _userType.rightView = dropDownPadding1 ;
    
    
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [dropDownView.view removeFromSuperview];
    [dropDownView closeAnimation];
    [self.view endEditing:YES];
    
    
    if (textField == _txtCountry) {
        
        [self.countryView setHidden:NO];
        
        [_lblBusinessDetailsTitle setHidden:YES];
        
        return NO;
        
        
    }else if (textField == _userType){
        
        
        dropDownView = [[DropDownView alloc] initWithArrayData:usrArray cellHeight:40 heightTableView:90 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
        
        
        
        dropDownView.delegate = self;
        
        dropDownView.view.backgroundColor = [UIColor colorWithRed:106/255.0f green:27/255.0 blue:154/255.0 alpha:1];
        
        
        [self.view addSubview:dropDownView.view];
        
        [self.view bringSubviewToFront:dropDownView.view];
        
        _dropDownTxtfield = _userType ;
        
        [ dropDownView openAnimation];
        
        return NO ;
        
        
        
    }
    else if (textField == _txtOperator){
        
        
        
        if (operatorArray.count >0) {
            
            [self   operatorDropDown];
            
        }else{
            [self  operatorGetApiFromServer];
        }
        
        return NO ;
        
        
        
        
    }
    else if (textField == _txtDOB) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }
    else if (textField == _txtMobile) {
        
        
    }
    
    
    
    return true;
}



-(void)dropDownCellSelected:(NSInteger)returnIndex{
    
    if (_dropDownTxtfield == _txtOperator) {
        
        _txtOperator.text = [operatorArray objectAtIndex:returnIndex];
        
    }
    else if (_dropDownTxtfield == _userType) {
        
        _userType.text = [usrArray objectAtIndex:returnIndex];
        
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField ==_txtName) {
        if (textField.text.length == 15 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    } else if (textField ==_txtMobile) {
        if (textField.text.length == 10 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}

- (IBAction)dateDoneClicked:(id)sender {
    [self ShowSelectedDate];
}




-(void)ShowSelectedDate{
    
    NSDateFormatter *Df = [[NSDateFormatter alloc]init];
    [Df setDateFormat:@"yyyy-MM-dd"];
    NSString*dateString =[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicker.date]];
    self.txtDOB.text = dateString ;
    NSDate *dateFromString = [[NSDate alloc] init];
    Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFromString = [Df dateFromString:dateString];
    
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
    dtvalue = [dtTime integerValue];
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomDate.constant = 270;
        [self.view layoutSubviews];
        
    }];
}


-(BOOL)emailStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}


- (IBAction)maleClicked:(id)sender {
    
    [_btnMale setSelected:YES];
    [_btnFemale setSelected:NO];
    gender = @"Male";
    
}

- (IBAction)femaleClicked:(id)sender {
    [_btnMale setSelected:NO];
    [_btnFemale setSelected:YES];
    gender = @"Female";
}

-(void)loginApiToServer {
    
    
    NSString* Identifie = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", Identifie);
    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    
    
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/unsecure/usr/reg"];
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:_txtCountry.text forKey:@"country"];
    [_params setObject:_txtName.text forKey:@"firstNm"];
    [_params setObject:_txtLastName.text forKey:@"lastNm"];
    [_params setObject:_txtMobile.text forKey:@"mobile"];
    [_params setObject:_txtGmail.text forKey:@"email"];
    [_params setObject:_userType.text forKey:@"userType"];
    [_params setObject:[NSString stringWithFormat:@"%ld",dtvalue] forKey:@"dob"];
    
    [_params setObject:@"Airtel" forKey:@"oprt"];
    
    
    
    
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    
    NSString* profileKey = @"prflUrl";
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSData *profiledata = UIImageJPEGRepresentation(checqImage, 1.0);
    
    if (profiledata) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", profileKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:profiledata];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      
                                      [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
                                      
                                      
                                      NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                      
                                      
                                      if(data!=nil){
                                          loginData =  [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
                                          
                                          NSLog(@"%@",loginData);
                                          
                                          [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
                                          
                                      }else{
                                          
                                          UIAlertController * alert = [UIAlertController
                                                                       alertControllerWithTitle:nil
                                                                       message:@"Error From Server"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction* noButton = [UIAlertAction
                                                                     actionWithTitle:@"Ok"
                                                                     style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         
                                                                     }];
                                          
                                          [alert addAction:noButton];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                      
                                      
                                      
                                      
                                      
                                  }];
    
    [task resume];
    
}



-(void)checkResponse{
    
    
    NSString*UserType = _userType.text ;
    
    [[NSUserDefaults standardUserDefaults]setObject:UserType forKey:@"UserType"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
    if ([[loginData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        
        
        
        NSString*userId =[NSString stringWithFormat:@"%@",[loginData valueForKey:@"usrId"]];
        
        NSString*otpVrfy =[NSString stringWithFormat:@"%@",[loginData valueForKey:@"otpVrfy"]];
        
        NSString*passvrfy = [NSString stringWithFormat:@"%@",[loginData valueForKey:@"passVrfy"]];
        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:userId forKey:@"id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [_otpView setHidden:NO];
        
        //        OtpVc *Cvc = [[OtpVc alloc]init];
        //        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"OtpVc"];
        //
        //
        //        Cvc.mobile = _txtMobile.text ;
        //        [self.navigationController pushViewController:Cvc animated:YES];
        
        
        
    }
    else if ([[loginData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:403]]){
        
        NSString*userId =[NSString stringWithFormat:@"%@",[loginData valueForKey:@"usrId"]];
        [[NSUserDefaults standardUserDefaults]setObject:userId forKey:@"id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSString*otpVrfy =[NSString stringWithFormat:@"%@",[loginData valueForKey:@"otpVrfy"]];
        NSString*passvrfy = [NSString stringWithFormat:@"%@",[loginData valueForKey:@"passVrfy"]];
        
        if ([otpVrfy isEqualToString:@"0"]) {
            
            [_otpView setHidden:NO];
            
            //            OtpVc *Cvc = [[OtpVc alloc]init];
            //            Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"OtpVc"];
            //            Cvc.mobile = _txtMobile.text ;
            //            [self.navigationController pushViewController:Cvc animated:YES];
        }else if ([otpVrfy isEqualToString:@"1"] && [passvrfy isEqualToString:@"0"]){
            
            
            CreatePasswordVc * viewStep = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
            
            [self.navigationController pushViewController:viewStep animated:YES];
            
        }
        
    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[loginData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [countryArray count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _countryTableView) {
        CountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"CountryCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        //        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        //        if (cell == nil) {
        //            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        //        }
        
        if([self.checkedIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.countryName.text = [[countryArray valueForKey:@"name"]objectAtIndex:indexPath.row];
        
        [cell.countryImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[countryArray  valueForKey:@"flagUrl"]objectAtIndex:indexPath.row]]] placeholderImage:nil];
        
        
        
        return cell;
        
        
    }
    return 0;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if(self.checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [tableView
                                        cellForRowAtIndexPath:self.checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    if([self.checkedIndexPath isEqual:indexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.checkedIndexPath = indexPath;
    }
    
    
    
    
    
    self.txtCountry.text = [[countryArray valueForKey:@"name"] objectAtIndex:indexPath.row];
    
    cuCode = [[countryArray valueForKey:@"code"] objectAtIndex:indexPath.row];
    
    [countryImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[countryArray valueForKey:@"flagUrl"]objectAtIndex:indexPath.row]]] placeholderImage:nil];
    
    [self.searchBar resignFirstResponder];
    [self.countryView setHidden:YES];
    [_lblBusinessDetailsTitle setHidden:NO];
    
    
}

-(void)CountryApiFromServer{
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://192.168.1.95:8080/dndFilter/location/country"];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        countryData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(countryMessage) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        
        
    }];
    
    
}
-(void)kvnDismiss{
    [KVNProgress dismiss];
    
}


-(void)countryMessage {
    
    if ([[countryData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        countryArray = [countryData valueForKey:@"countryList"];
        
        [self.countryTableView reloadData];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error From Server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.searchBar.showsCancelButton = true;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    self.checkedIndexPath = nil;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains %@", searchText];
    countryArray = [countryData valueForKey:@"countryList"];
    NSArray *filteredArray = [countryArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count != 0) {
        countryArray = [[NSMutableArray alloc]initWithArray:filteredArray];
    }
    
    
    [self.countryTableView reloadData];
}




-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.searchBar resignFirstResponder];
    self.searchBar.showsCancelButton = false;
    [self.countryView setHidden:YES];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    
}

////////////////////////////////operator Api////////////////////////////////////////////

-(void)operatorGetApiFromServer{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/data/operators"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    //    NSString*consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"CosumerId"];
    
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         operatorData = responseObject;
         
         [self performSelectorOnMainThread:@selector(operatorResponse) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
              
          }];
    
}


-(void)operatorResponse{
    
    
    operatorArray = [operatorData valueForKey:@"oprtrNm"];
    
    [self operatorDropDown];
    
    
}


-(void)operatorDropDown{
    
    dropDownView = [[DropDownView alloc] initWithArrayData:operatorArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:_txtOperator animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
    
    
    
    dropDownView.delegate = self;
    dropDownView.view.backgroundColor = [UIColor colorWithRed:245/255.0f green:115/255.0 blue:115/255.0 alpha:1];
    
    [self.view addSubview:dropDownView.view];
    
    [self.view bringSubviewToFront:dropDownView.view];
    
    _dropDownTxtfield = _txtOperator ;
    
    [ dropDownView openAnimation];
    
}

-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}


///upload

- (IBAction)uploadImg:(id)sender{
    
    [self requestAuthorizationWithRedirectionToSettings];
    
    
}
- (IBAction)cameraClicked:(id)sender{
    [self requestAuthorizationWithRedirectionToSettings];
}

-(void)setAlertCtrl;
{
    self.AlertCtrl = [UIAlertController alertControllerWithTitle:@"selectImage" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             
                             {
                                 [self camera];
                                 
                                 isCamera = true;
                                 
                                 
                             }];
    
    UIAlertAction *Library  = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action)
                               
                               {
                                   
                                   [self selectPhoto];
                                   
                                   isCamera = false;
                               }];
    
    UIAlertAction *Cancel  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        isCamera = false;
        
    }];
    
    [self.AlertCtrl addAction:camera];
    [self.AlertCtrl addAction:Library];
    [self.AlertCtrl addAction:Cancel];
    
}

- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

-(void)camera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        
    } else {
        
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    
    if (isCamera == true) {
        
        checqImage = info[UIImagePickerControllerEditedImage];
        
        
        _imgCamera.image = checqImage ;
        
        
        NSData *webData = UIImagePNGRepresentation(checqImage);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        self.txtImgName.text = documentsDirectory ;
        UIImageWriteToSavedPhotosAlbum(checqImage, nil, nil, nil);
        
        
        
    }else
    {
        
        
        checqImage = info[UIImagePickerControllerEditedImage];
        
        _imgCamera.image = checqImage ;
        
        NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
        NSString *filename = [[result firstObject] filename];
        self.txtImgName.text = filename ;
        
        
    }
    
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)requestAuthorizationWithRedirectionToSettings {
    dispatch_async(dispatch_get_main_queue(), ^{
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusAuthorized)
        {
            isCamera = false ;
            
           // if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                UIPopoverPresentationController *popPresenter = [_AlertCtrl popoverPresentationController];
                
                popPresenter.sourceView = self.view;
                
                popPresenter.sourceRect = self.view.bounds;
                
                [self presentViewController:_AlertCtrl animated:YES completion:nil];
                
           // }else{
                
                [self presentViewController:self.AlertCtrl animated:YES completion:nil];
                
           // }
        }
        else
        {
            //No permission. Trying to normally request it
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status != PHAuthorizationStatusAuthorized)
                {
                    //User don't give us permission. Showing alert with redirection to settings
                    //Getting description string from info.plist file
                    NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:cancelAction];
                    
                    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                    }];
                    [alertController addAction:settingsAction];
                    
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                }
            }];
        }
    });
}



/////////////////////////////////////////otp Screen ///////////////////////////

- (IBAction)VarifyClicked:(id)sender {
    
    [KVNProgress show];
    [self  verifyOtpFromApi];
    
    
    
}

- (IBAction)resendOtpClicked:(id)sender{
    
    [KVNProgress show];
    [self ResendOtpFromApi];
    
}

- (IBAction)bckOtp:(id)sender {
    
    [self.otpView setHidden:YES];
    
    
}




/////////////// verify clicked api ////////////////////

-(void)verifyOtpFromApi{
    
    //    "private String mobile;
    //    private int otp;
    //    private long usrId;
    //    private String action; //Verify,Resend"
    userId = [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//unsecure/otp"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobile" :_txtMobile.text,
                @"otp" :_txtOtp.text,
                @"usrId" :userId,
                @"action" : @"Verify"
                
                };
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         verifiedData = responseObject;
         [self performSelectorOnMainThread:@selector(goToPasswordView) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Server error"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
         
         
         
     }];
    
    
    
}

-(void)goToPasswordView {
    
    if ([[verifiedData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        CreatePasswordVc *Cvc = [[CreatePasswordVc alloc]init];
        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
        
        [self.navigationController pushViewController:Cvc animated:YES];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[verifiedData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

/////////////////// ResendClicked api ///////////////////////////////////////////////////

-(void)ResendOtpFromApi{
    
    userId = [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//unsecure/otp"];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobileNo" :_txtMobile.text,
                @"regotp" :_txtOtp.text,
                @"usrId" :userId,
                @"action" : @"Resend"
                
                };
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         resendData = responseObject;
         [self performSelectorOnMainThread:@selector(resendOtpResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Server Error"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
         
     }];
    
    
    
}

-(void)resendOtpResponse {
    
    if ([[resendData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        NSLog(@"success");
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[resendData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}





- (IBAction)backCountryClicked:(id)sender {
    [self.countryView setHidden:YES];
}



-(void)UpdateApiToServer{
    
  
  
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/update"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    //    NSString*consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
  
    
  
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    _params = @{
                @"usrId" : userId ,
                @"email" :_txtGmail.text,
                @"lastNm" : _txtLastName .text,
                @"firstNm" :_txtName.text
                
                };
    
    
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
         updateData = responseObject;
         [self performSelectorOnMainThread:@selector(updateResponse) withObject:nil waitUntilDone:YES];
         
         
     }
           failure:^(NSURLSessionTask *operation, NSError *error) {
               [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
               UIAlertController * alert = [UIAlertController
                                            alertControllerWithTitle:nil
                                            message:@"Server error"
                                            preferredStyle:UIAlertControllerStyleAlert];
               UIAlertAction* noButton = [UIAlertAction
                                          actionWithTitle:@"Ok"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              
                                          }];
               
               [alert addAction:noButton];
               [self presentViewController:alert animated:YES completion:nil];
               
               
           }];
    
}

-(void)updateResponse{
    
    
   
    
    
    if ([[updateData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
       
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
        
    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[updateData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

@end
