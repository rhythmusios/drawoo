//
//  UpdateVc.h
//  Swipeler
//
//  Created by katoch on 19/01/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtpVc.h"
#import "KVNProgress.h"
#import "CountryCell.h"
#import "UIImageView+WebCache.h"
#import "DropDownView.h"
#import "CreatePasswordVc.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface UpdateVc : UIViewController{
     NSInteger dtvalue ;
    
    NSMutableArray*dropDownArray;
    DropDownView *dropDownView ;
    NSString *userType ;
    
    id countryData;
    NSMutableArray*countryList ;
    UIImageView *countryImage;
    NSMutableArray*countryArray;
    NSString*cuCode;
   
    
    NSMutableArray *merchantArray;
    NSMutableArray*businessCategory;
    NSMutableArray*SubCategory;
    NSMutableArray*proprietorArray;
    
    NSString*gender;
    id loginData;
    
    NSMutableArray*usrArray;
    NSMutableArray*operatorArray;
    id operatorData;
    
    UIImage*checqImage;
    BOOL isCamera ;
    
    ////otp//////
    id resendData;
    id verifiedData;
    
    NSString* userId;
    
    id updateData;


    
}

@property (strong,nonatomic) NSString *emailId ;
@property (strong,nonatomic) NSString *mobileNumber ;
@property (strong,nonatomic) NSString *type ;
@property (strong,nonatomic) NSString *firstName ;
@property (strong,nonatomic) NSString *secondName ;
@property (strong,nonatomic)NSString *dateOb ;






@property (strong, nonatomic) IBOutlet UIAlertController *AlertCtrl;

@property (strong, nonatomic) IBOutlet UITextField *userType;

@property (strong, nonatomic) IBOutlet UITextField *txtOperator;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *countryTableView;
@property (strong, nonatomic) IBOutlet UIView *countryView;

@property (strong, nonatomic) IBOutlet UILabel *lblBusinessDetailsTitle;

@property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@property (strong, nonatomic) UITextField *dropDownTxtfield;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;

@property (strong, nonatomic) IBOutlet UITextField *txtDOB;

@property (strong, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtGmail;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomDate;

@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtGender;

@property (strong, nonatomic) IBOutlet UIButton *btnMale;
@property (strong, nonatomic) IBOutlet UIButton *btnFemale;


- (IBAction)maleClicked:(id)sender;
- (IBAction)femaleClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;

@property (strong, nonatomic) IBOutlet UIImageView *imgCamera;
@property (strong, nonatomic) IBOutlet UILabel *txtImgName;



- (IBAction)loginClicked:(id)sender;
- (IBAction)nextClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)dateDoneClicked:(id)sender;

- (IBAction)uploadImg:(id)sender;
- (IBAction)cameraClicked:(id)sender;



////////////////// otp /////////////////////

- (IBAction)VarifyClicked:(id _Nullable )sender;
- (IBAction)resendOtpClicked:(id _Nullable )sender;
- (IBAction)bckOtp:(id _Nullable )sender;


@property(strong,nonatomic)NSString * _Nonnull otpStr;
@property(strong,nonatomic)NSString * _Nonnull mobileStr;
@property(strong,nonatomic)NSString * _Nonnull token;

@property (strong, nonatomic) IBOutlet UITextField * _Nullable txtOtp;
@property (strong, nonatomic) IBOutlet UIButton * _Nullable btnVarify;

@property (strong, nonatomic)NSString * _Nonnull mobile;
@property (strong, nonatomic) IBOutlet UIView * _Nullable otpView;
@property (strong, nonatomic) IBOutlet UIView * _Nonnull viewImage;
- (IBAction)backCountryClicked:(id)sender;


@end
