//
//  SideHistoryVc.m
//  Pene
//
//  Created by katoch on 07/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "SideHistoryVc.h"
#import "lottryProfileVc.h"
#import "KVNProgress.h"
#import <AFNetworking/AFNetworking.h>
#import "HistoryCell.h"

@interface SideHistoryVc ()

@end

@implementation SideHistoryVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _txtStartDate.text = @"27-Feb-2018 00:00";
    _txtEndDate.text = @"27-Feb-2018 23:59";
    
    //t1
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [gregorian setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDateComponents *components = [gregorian components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *beginningOfToday = [gregorian dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    NSString *currentDate = [dateFormatter stringFromDate:beginningOfToday];
    
    _txtStartDate.text = currentDate ;
    
    
    NSTimeInterval timeInMiliseconds = [beginningOfToday timeIntervalSince1970]*1000;
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
    dtvalue = [dtTime integerValue];
    
    //t2
    NSDateComponents *components2= [[NSDateComponents alloc] init];
    [components2 setHour:23];
    [components2 setMinute:59];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *myNewDate=[calendar dateByAddingComponents:components2 toDate:beginningOfToday options:0];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd-MMM-yyyy HH:mm"];
    [dateFormatter1 setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    NSString *ENDDate1 = [dateFormatter1 stringFromDate:myNewDate];
    
    _txtEndDate.text = ENDDate1 ;

    
    NSTimeInterval tomorrowMiliseconds = [myNewDate timeIntervalSince1970]*1000;
    NSString*tomTime = [NSString stringWithFormat:@"%f",tomorrowMiliseconds];
    endvalue = [tomTime integerValue];
    
    
    
    
    
    
    _SideImage.layer.cornerRadius = self.SideImage.frame.size.width/2 ;
    _SideImage.layer.masksToBounds = YES;
    
    _Viewimage.layer.cornerRadius = self.Viewimage.frame.size.width/2 ;
    _Viewimage.layer.masksToBounds = YES;
    
    
    
    
    _Viewheader.layer.borderColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.2].CGColor;
    _Viewheader.layer.borderWidth = 2.0f;
    
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.viewMenuSlide addGestureRecognizer:gesRecognizer4];
    
    menuItemsSection1 = [[NSMutableArray alloc]initWithObjects:@"Profile",@"How to play",@"About us",@"Contact us",@"History",@"Privacy Policy",@"Terms & Conditions",@"FAQ",@"Logout",nil];
    sectionImages1 = [[NSMutableArray alloc]initWithObjects:@"avatar",@"play",@"aboutUs",@"contactUs",@"history-symbol-of-antique-building (3)",@"privacy-60",@"terms",@"aboutUs",@"logout-60", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClicked:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.lblNoDataFoundHTV setHidden:YES];
    [self.lblNoDataFoundDTV setHidden:YES];
    [self.lblNoDataFoundJTV setHidden:YES];
    
    userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    [_btnHourly setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
    
     [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    
    _viewMenuSlide.hidden = true;
    
    [KVNProgress show];
    [self  drawDataToServer];
    
    
//        [self HistoryBidsGetApi];
   
    
    
    
    
}






- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView == _hourlyTableView)
    {
        return hourlydata.count;
        
    }else if (tableView == _DailyTableView)
    {
        return dailyData.count;
    }else if (tableView == _jackpotTableview)
    {
        return jackpotData.count;
        
    }else if (tableView == _sideTableView)
    {
        return menuItemsSection1.count;
    }
    else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    NSString *cellid = @"HistoryCell";
    
    if (tableView == _hourlyTableView)
    {
        
        HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        
        if (cell == nil)
            
        {
            
            NSArray * cellarray = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
            
            cell = cellarray[0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        
        
        cell.txtBetNo.text =[NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
        
        NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
        
        NSInteger bidend = [BidEndStr integerValue];
        NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSString *BidDrawDate = [formatter stringFromDate:drwDate];
        cell.txtDate .text =  BidDrawDate ;
        
        cell.txtAmount.text = [NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
        
        
        cell.txtStatus.text = [NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"result"]objectAtIndex:indexPath.row]];
        
        NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[hourlydata valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
        if ([winStr isEqualToString:@"<null>"] || winStr == nil) {
            cell.txtWininigNo.text  = @"Scheduled";
        }
        else{
            cell.txtWininigNo.text  = winStr ;
            
        }
        
        
        return cell;
    }
    
    else if (tableView == _DailyTableView)
    {
        HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        
        if (cell == nil)
            
        {
            
            NSArray * cellarray = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
            
            cell = cellarray[0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        
        
        cell.txtBetNo.text =[NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
        
        NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
        
        NSInteger bidend = [BidEndStr integerValue];
        NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSString *BidDrawDate = [formatter stringFromDate:drwDate];
        cell.txtDate .text =  BidDrawDate ;
        
        cell.txtAmount.text = [NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
        
        
        cell.txtStatus.text = [NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"result"]objectAtIndex:indexPath.row]];
        
        NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[dailyData valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
        if ([winStr isEqualToString:@"<null>"] || winStr == nil) {
            cell.txtWininigNo.text  = @"Scheduled";
        }
        else{
            cell.txtWininigNo.text  = winStr ;
            
        }
        
        
        return cell;
    }else if (tableView == _jackpotTableview)
    {
        HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        
        if (cell == nil)
            
        {
            
            NSArray * cellarray = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
            
            cell = cellarray[0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        
        
        cell.txtBetNo.text =[NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
        
        NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
        
        NSInteger bidend = [BidEndStr integerValue];
        NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSString *BidDrawDate = [formatter stringFromDate:drwDate];
        cell.txtDate .text =  BidDrawDate ;
        
        cell.txtAmount.text = [NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
        
        
        cell.txtStatus.text = [NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"result"]objectAtIndex:indexPath.row]];
        
        NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[jackpotData valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
        if ([winStr isEqualToString:@"<null>"] || winStr == nil) {
            cell.txtWininigNo.text  = @"Scheduled";
        }
        else{
            cell.txtWininigNo.text  = winStr ;
            
        }
        return cell;
        
    }
    
    else{
        return 0;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _sideTableView) {
        return 45;
    }else{
        return 51;
        
    }
}


-(void)edit:(id)sender{
    UIButton *btnTag = (UIButton*)sender;
    NSLog(@"%ld",(long)btnTag.tag);
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    NSInteger pagenumber = scrollView.contentOffset.x / scrollView.bounds.size.width;
    
    NSLog(@"Page number is %li", (long)pagenumber);
    if (pagenumber==1) {
        
        _lblLeadingConstant.constant = self.btnDaily.frame.size.width;
        
//        [_btnDaily setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//        [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//        [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
        
        [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
    }else if (pagenumber==2)
    {
        
        _lblLeadingConstant.constant = self.btnJackpot.frame.size.width + self.btnJackpot.frame.size.width ;
        [_btnJackpot setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//        [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//        [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
        
        [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        
    }else if (pagenumber==0)
    {
        [_btnHourly setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//        [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//        [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
        [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        _lblLeadingConstant.constant = 0;
        [self.view layoutIfNeeded];
        
    }
    
}







- (IBAction)hourlyClicked:(id)sender
{
    
    [_scrollview setContentOffset:CGPointMake(0, 0)animated:YES];
    
    [_btnHourly setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
    
    [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    
    
    
}

- (IBAction)dailyClicked:(id)sender
{
    
    
    [_scrollview setContentOffset:CGPointMake(self.scrollview.frame.size.width, 0)animated:YES];
    
    
    [_btnDaily setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
    
//    [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
    
    [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
}


- (IBAction)jackpotClicked:(id)sender {
    
    [_btnJackpot setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
    
//    [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//
    [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    
    [_scrollview setContentOffset:CGPointMake(self.scrollview.frame.size.width*2, 0)animated:YES];
    
}





-(void)tapGesture{
    
    [dropDownView closeAnimation];
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        _viewMenuSlide.hidden = true;
        
    }];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }
    else if ([touch.view isDescendantOfView:_sideTableView]) {
        
        
        
        return NO;
    }
    
    return YES;
}





-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}




///////////history api //////////////

-(void)drawDataToServer{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//secure/drw/history/%@?&t1=%ld&t2=%ld",userid,(long)dtvalue,(long)endvalue];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
  
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         historyData = responseObject;
         [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              UIAlertController * alert = [UIAlertController
                                           alertControllerWithTitle:nil
                                           message:@"Server error"
                                           preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction* noButton = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             
                                         }];
              
              [alert addAction:noButton];
              [self presentViewController:alert animated:YES completion:nil];
              
              
          }];
    
}

-(void)checkResponse{
    
    
    
    
    
    result = [[NSMutableArray alloc]init];
    
    
    if ([[historyData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        
        NSString*hourly = @"HOURLY" ;
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.schdlTyp contains %@",
                                  hourly];
        
        hourlydata = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicate];
        
        
        
        NSString*DAILY = @"DAILY" ;
        
        NSPredicate *predicateDaily = [NSPredicate
                                       predicateWithFormat:@"SELF.schdlTyp contains %@",
                                       DAILY];
        
        dailyData = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicateDaily];
        
        
        
        NSString*JACKPOT = @"JACKPOT" ;
        NSPredicate *predJACKPOT = [NSPredicate
                                    predicateWithFormat:@"SELF.schdlTyp contains %@",
                                    JACKPOT];
        
        jackpotData = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predJACKPOT];
        
        if (hourlydata.count > 0) {
            [self.hourlyTableView setHidden:NO];
        }else{
            
            [self.hourlyTableView setHidden:YES];
            [self.lblNoDataFoundHTV setHidden:NO];
        }
        
        
        if (dailyData.count > 0) {
            [self.DailyTableView setHidden:NO];
            
        }
        else{
            [self.DailyTableView setHidden:YES];
            [self.lblNoDataFoundDTV setHidden:NO];
        }
        
        if (jackpotData.count > 0) {
            [self.jackpotTableview setHidden:NO];
        }
        else{
            [self.jackpotTableview setHidden:YES];
            [self.lblNoDataFoundJTV setHidden:NO];
        }
        
        
        
        [self.hourlyTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
         [self.DailyTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
         [self.jackpotTableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
            
        }
    
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[historyData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



- (IBAction)doneClicked:(id)sender {
    
    [self ShowSelectedDate];
    
    
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    if (textField == _txtStartDate) {
        
        
        checkDateStr = @"Start";
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        _txtEndDate.text = @"" ;
        
        return NO;
    }else if (textField == _txtEndDate) {
        checkDateStr = @"End";
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
            
        }];
        
        return NO;
    }
    
    return true;
}

-(void)ShowSelectedDate{
    
    
    
    NSDateFormatter *Df = [[NSDateFormatter alloc]init];
    [Df setDateFormat:@"dd-MMM-yyyy HH:mm"];
    NSString*dateString =[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicker.date]];
    
    if ([checkDateStr isEqualToString: @"Start"]) {
        _txtStartDate.text = dateString ;
    }else{
        
        _txtEndDate.text = dateString ;
    }
    
    NSDate *dateFromString = [[NSDate alloc] init];
    Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFromString = [Df dateFromString:dateString];
    
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
    if ([checkDateStr isEqualToString: @"Start"]) {
        dtvalue = [dtTime integerValue];
    }else{
        
        endvalue = [dtTime integerValue];
        
    }
    
    
    
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomDate.constant = -300;
        [self.view layoutSubviews];
        
    }];
    
    
    if ([checkDateStr isEqualToString:@"End"]) {
         [KVNProgress show];
        [self drawDataToServer];
        
    }
}

//
//-(void)HistoryBidsGetApi{
//    //    http://52.172.26.198:8082/drawoapp//secure/drw/bids/1000?page=0&size=10
//
//    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//secure/drw/bids/%@?page=0&size=10",userid];
//
//
//    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
//    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
//
//    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
//
//     {
//         NSLog(@"PLIST: %@", responseObject);
//         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
//
//         historyData = responseObject;
//
//         [self performSelectorOnMainThread:@selector(historyData) withObject:nil waitUntilDone:YES];
//
//
//     }
//          failure:^(NSURLSessionTask *operation, NSError *error) {
//              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
//              UIAlertController * alert = [UIAlertController
//                                           alertControllerWithTitle:nil
//                                           message:@"Server Error"
//                                           preferredStyle:UIAlertControllerStyleAlert];
//              UIAlertAction* noButton = [UIAlertAction
//                                         actionWithTitle:@"Ok"
//                                         style:UIAlertActionStyleDefault
//                                         handler:^(UIAlertAction * action) {
//
//                                         }];
//
//              [alert addAction:noButton];
//              [self presentViewController:alert animated:YES completion:nil];
//
//
//
//          }];
//
//
//
//}
//
//-(void)historyData{
//
//
//
//
//    if ([[historyData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
//
//
//        NSLog(@"%@",historyData);
//
//
//
//        NSString*hourly = @"HOURLY" ;
//
//        NSPredicate *predicate = [NSPredicate
//                                  predicateWithFormat:@"SELF.schdlTyp contains %@",
//                                  hourly];
//
//        hourlydata = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicate];
//
//
//
//        NSString*DAILY = @"DAILY" ;
//
//        NSPredicate *predicateDaily = [NSPredicate
//                                       predicateWithFormat:@"SELF.schdlTyp contains %@",
//                                       DAILY];
//
//        dailyData = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicateDaily];
//
//
//
//        NSString*JACKPOT = @"JACKPOT" ;
//        NSPredicate *predJACKPOT = [NSPredicate
//                                    predicateWithFormat:@"SELF.schdlTyp contains %@",
//                                    JACKPOT];
//
//        jackpotData = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predJACKPOT];
//
//
//
//
//        [self.hourlyTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
//
//        [self.DailyTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
//
//        [self.jackpotTableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
//
//
//
//
//    }
//
//    else{
//
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:nil
//                                     message:[currentBetData valueForKey:@"message"]
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* noButton = [UIAlertAction
//                                   actionWithTitle:@"Ok"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//
//                                   }];
//
//        [alert addAction:noButton];
//        [self presentViewController:alert animated:YES completion:nil];
//
//    }
//
//
//}




@end
