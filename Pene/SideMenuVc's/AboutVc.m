//
//  AboutVc.m
//  Pene
//
//  Created by katoch on 07/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "AboutVc.h"

@interface AboutVc ()

@end

@implementation AboutVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://rhythmus.in/about.html"]]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClicked:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

@end
