//
//  sideMenuCell.h
//  CityBus
//
//  Created by katoch on 29/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sideMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *menuImg;
@property (strong, nonatomic) IBOutlet UILabel *txtMenu;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xMore;

@end
