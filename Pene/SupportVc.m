//
//  SupportVc.m
//  FonadaApp
//
//  Created by katoch on 18/08/17.
//  Copyright © 2017 Rhythmus. All rights reserved.
//

#import "SupportVc.h"
#import "AppDelegate.h"
#import "HistoryCell.h"
#import "DropDownView.h"
#import "lottryProfileVc.h"
#import "KVNProgress.h"
#import "HistoryVc.h"


#import <AFNetworking/AFNetworking.h>
@interface SupportVc ()<UIScrollViewDelegate, UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
   
    NSMutableArray* SenderIdDta;
    NSMutableArray * TemplateIdDta;
    NSMutableArray* CreditIdDta;
    id loginData;
    NSString *strCmpId;
    NSString *strUserId;
    NSArray * array;
    NSMutableArray* templateArray;
    NSMutableArray *dateTimeArray;
    NSArray * arrlist;
    NSMutableArray* CreditArray;
    NSMutableArray *CreditDateTimeArr;
    int CellTxtSize;
    BOOL TemplateScroll;
    BOOL CreditScroll;
}


@end


@implementation SupportVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_sideTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    _SideImage.layer.cornerRadius = self.SideImage.frame.size.width/2 ;
    _SideImage.layer.masksToBounds = YES;
    
    _Viewimage.layer.cornerRadius = self.Viewimage.frame.size.width/2 ;
    _Viewimage.layer.masksToBounds = YES;
    
    
  

    _Viewheader.layer.borderColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.2].CGColor;
    _Viewheader.layer.borderWidth = 2.0f;
    
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.viewMenuSlide addGestureRecognizer:gesRecognizer4];
    
    menuItemsSection1 = [[NSMutableArray alloc]initWithObjects:@"Profile",@"About us",@"Contact us",@"History",@"Privacy Policy",@"Terms & Conditions",@"Logout",nil];
  sectionImages1 = [[NSMutableArray alloc]initWithObjects:@"avatar",@"aboutUs",@"contactUs",@"history-symbol-of-antique-building (3)",@"privacy-60",@"terms",@"aboutUs",@"logout-60", nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.lblNoDataFoundHTV setHidden:YES];
    [self.lblNoDataFoundDTV setHidden:YES];
    [self.lblNoDataFoundJTV setHidden:YES];
    
      userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];
    
    [_btnHourly setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];

    [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    
    
    _viewMenuSlide.hidden = true;
    [self profileApiFromServer];
    
    [KVNProgress show];
        [self CurrentBidGetApi];
    
    
   
    
//    [self.btnSender setTitleColor:[UIColor colorWithRed:0.14 green:0.59 blue:0.67 alpha:1.0] forState:UIControlStateNormal];
//    [self.btnTemplate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.btnCredits setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//
//    self.btnSender.backgroundColor = [UIColor whiteColor];
//    self.btnTemplate.backgroundColor = [UIColor clearColor];
//    self.btnCredits.backgroundColor = [UIColor clearColor];

    
    
    

}






- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView == _hourlyTableView)
    {
            return hourlydata.count;
        
    }else if (tableView == _DailyTableView)
    {
        return dailyData.count;
    }else if (tableView == _jackpotTableview)
    {
        return jackpotData.count;
    }else if (tableView == _sideTableView)
    {
        return menuItemsSection1.count;
    }
    else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    NSString *cellid = @"HistoryCell";
    
    if (tableView == _hourlyTableView)
    {
    
    HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    
    if (cell == nil)
        
    {
        
        NSArray * cellarray = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
        
        cell = cellarray[0];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        

        cell.txtBetNo.text =[NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
        
        NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
        
        NSInteger bidend = [BidEndStr integerValue];
        NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSString *BidDrawDate = [formatter stringFromDate:drwDate];
        cell.txtDate .text =  BidDrawDate ;
        
         cell.txtAmount.text = [NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
        
        
          cell.txtStatus.text = [NSString stringWithFormat:@"%@",[[hourlydata valueForKey:@"result"]objectAtIndex:indexPath.row]];
        
        
         NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[hourlydata valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
        
        if ([winStr isEqualToString:@"<null>"] || winStr == nil || [winStr isEqualToString:@"(null)"]||[winStr isEqualToString:@"null"]) {
            cell.txtWininigNo.text  = @"Scheduled";
        }
        else{
            cell.txtWininigNo.text  = winStr ;
            
        }
    
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        
        return cell;
    }
    
    else if (tableView == _DailyTableView)
    {
        HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        
        if (cell == nil)
            
        {
            
            NSArray * cellarray = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
            
            cell = cellarray[0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        
        cell.txtBetNo.text =[NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
        
        NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
        
        NSInteger bidend = [BidEndStr integerValue];
        NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSString *BidDrawDate = [formatter stringFromDate:drwDate];
        cell.txtDate .text =  BidDrawDate ;
        
        cell.txtAmount.text = [NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
        
        
        cell.txtStatus.text = [NSString stringWithFormat:@"%@",[[dailyData valueForKey:@"result"]objectAtIndex:indexPath.row]];
        
        NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[dailyData valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
        
        if ([winStr isEqualToString:@"<null>"] || winStr == nil || [winStr isEqualToString:@"(null)"]||[winStr isEqualToString:@"null"]) {
            cell.txtWininigNo.text  = @"Scheduled";
        }
        else{
            cell.txtWininigNo.text  = winStr ;
            
        }
        
        
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        return cell;
    }else if (tableView == _jackpotTableview)
    {
        HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        
        if (cell == nil)
            
        {
            
            NSArray * cellarray = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
            
            cell = cellarray[0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
        }
        
        cell.txtBetNo.text =[NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
        
        NSString*BidEndStr =  [NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
        
        NSInteger bidend = [BidEndStr integerValue];
        NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yyyy"];
        //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        NSString *BidDrawDate = [formatter stringFromDate:drwDate];
        cell.txtDate .text =  BidDrawDate ;
        
        cell.txtAmount.text = [NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
        
        
        cell.txtStatus.text = [NSString stringWithFormat:@"%@",[[jackpotData valueForKey:@"result"]objectAtIndex:indexPath.row]];
        
        NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[jackpotData valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
        
        if ([winStr isEqualToString:@"<null>"] || winStr == nil || [winStr isEqualToString:@"(null)"]||[winStr isEqualToString:@"null"]) {
            cell.txtWininigNo.text  = @"Scheduled";
        }
        else{
            cell.txtWininigNo.text  = winStr ;
            
        }
        
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        return cell;
        
    }
   else if (tableView== _sideTableView){
        sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
        
        cell.txtMenu.text  = [menuItemsSection1 objectAtIndex:indexPath.row];
        cell.menuImg.image = [UIImage imageNamed:[sectionImages1 objectAtIndex:indexPath.row]];
        
        
        
        
        return cell;
        
        
    }
   else{
    return 0;
       
   }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (tableView == _sideTableView) {
        
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            return 45;
        }else{
            return 65 ;
            
        }
    }else{
    return 51;
        
    }
}


-(void)edit:(id)sender{
    UIButton *btnTag = (UIButton*)sender;
    NSLog(@"%ld",(long)btnTag.tag);
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    

        NSInteger pagenumber = scrollView.contentOffset.x / scrollView.bounds.size.width;
        
        NSLog(@"Page number is %li", (long)pagenumber);
        if (pagenumber==1) {
            
            _lblLeadingConstant.constant = self.btnDaily.frame.size.width;
            
            [_btnDaily setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//            [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//            [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];

            [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            
        }else if (pagenumber==2)
        {
            
            _lblLeadingConstant.constant = self.btnJackpot.frame.size.width + self.btnJackpot.frame.size.width ;
            [_btnJackpot setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//            [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//            [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
            [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            
           
            
        }else if (pagenumber==0)
        {
            [_btnHourly setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//            [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//            [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
            [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            

            _lblLeadingConstant.constant = 0;
            [self.view layoutIfNeeded];
            
        }
        
    }
    
    
    
    



- (IBAction)hourlyClicked:(id)sender
{

     [_scrollview setContentOffset:CGPointMake(0, 0)animated:YES];
    
    [_btnHourly setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];

    [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    

    
    
    
}

- (IBAction)dailyClicked:(id)sender
{
    
   
    [_scrollview setContentOffset:CGPointMake(self.scrollview.frame.size.width, 0)animated:YES];
  
    
    [_btnDaily setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];

//    [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnJackpot setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
    
    [_btnJackpot setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    

    
}


- (IBAction)jackpotClicked:(id)sender {
   
    [_btnJackpot setTitleColor:[UIColor colorWithRed:106/255.0f green:27/255.0f blue:154/255.0f alpha:1] forState:UIControlStateNormal];
    
//    [_btnHourly setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
//    [_btnDaily setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1] forState:UIControlStateNormal];
    
    
    [_btnDaily setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_btnHourly setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
  
    [_scrollview setContentOffset:CGPointMake(self.scrollview.frame.size.width*2, 0)animated:YES];
    
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)menuClicked:(id)sender {
    
    
    [self.view bringSubviewToFront:self.viewMenuSlide];
    _viewMenuSlide.hidden = false;
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = 0;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        //            _viewMenuSlide.hidden = true;
        
    }];
    
}

-(void)tapGesture{
    
    [dropDownView closeAnimation];
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        _viewMenuSlide.hidden = true;
        
    }];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }
    else if ([touch.view isDescendantOfView:_sideTableView]) {
        
        
        
        return NO;
    }
    
    return YES;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_sideTableView) {
        
        if ([menuItemsSection1[indexPath.row] isEqualToString:@"Profile"]) {
            lottryProfileVc*tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"lottryProfileVc"];
            
            
            
            [self showViewController:tbc sender:self];
            
            
        }
        //        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"How to play"])
        //        {
        //            PlayVc *play = [[PlayVc alloc]init];
        //            play = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVc"];
        //
        //
        //
        //  [self showViewController:play sender:self];
        //
        //
        //        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"About us"])
        {
            AboutVc *About = [[AboutVc alloc]init];
            About = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutVc"];
            
            [self showViewController:About sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Contact us"])
        {
            ContactVc *contact = [[ContactVc alloc]init];
            contact = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactVc"];
            
            [self showViewController:contact sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"History"])
        {
            SideHistoryVc *his = [[SideHistoryVc alloc]init];
            his = [self.storyboard instantiateViewControllerWithIdentifier:@"SideHistoryVc"];
            his.checkStr = @"History";
            
            [self showViewController:his sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Privacy Policy"])
        {
            PrivcyVc *privac = [[PrivcyVc alloc]init];
            privac = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcyVc"];
            
            [self showViewController:privac sender:self];
            
        }else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Terms & Conditions"])
        {
            TermsVc *term = [[TermsVc alloc]init];
            term = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsVc"];
            
            [self showViewController:term sender:self];
            
        }
        //        else if (indexPath.row == 7)
        //        {
        //            FaqVc *About = [[FaqVc alloc]init];
        //            About = [self.storyboard instantiateViewControllerWithIdentifier:@"FaqVc"];
        //
        //            [self showViewController:About sender:self];
        //
        //        }
        
        
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Logout"]) {
            UIAlertView*logout = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure you want to log out" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            logout.tag = 11 ;
            logout.delegate = self;
            [logout show];
        }
        
    }
    
    
}


-(void)CurrentBidGetApi{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//secure/drw/current/%@",userid];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
     //    [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         currentBetData = responseObject;
         
         [self performSelectorOnMainThread:@selector(currentBetData) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              UIAlertController * alert = [UIAlertController
                                           alertControllerWithTitle:nil
                                           message:@"Server Error"
                                           preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction* noButton = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             
                                         }];
              
              [alert addAction:noButton];
              [self presentViewController:alert animated:YES completion:nil];
              
              
              
          }];
    
    
    
}

-(void)currentBetData{
    
    
    
    if ([[currentBetData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        
        NSLog(@"%@",currentBetData);
        
        
        NSString*hourly = @"HOURLY" ;
        
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"SELF.schdlTyp contains %@",
                                  hourly];
        
        hourlydata = [[currentBetData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicate];
        
        
        
         NSString*DAILY = @"DAILY" ;
        
        NSPredicate *predicateDaily = [NSPredicate
                                  predicateWithFormat:@"SELF.schdlTyp contains %@",
                                  DAILY];
        
        dailyData = [[currentBetData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicateDaily];
        
        
        
         NSString*JACKPOT = @"JACKPOT" ;
        NSPredicate *predJACKPOT = [NSPredicate
                                       predicateWithFormat:@"SELF.schdlTyp contains %@",
                                       JACKPOT];
        
        jackpotData = [[currentBetData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predJACKPOT];
        
        
        if (hourlydata.count > 0) {
            [self.hourlyTableView setHidden:NO];
        }else{
            
            [self.hourlyTableView setHidden:YES];
            [self.lblNoDataFoundHTV setHidden:NO];
        }
        
        
        if (dailyData.count > 0) {
             [self.DailyTableView setHidden:NO];
            
        }
        else{
            [self.DailyTableView setHidden:YES];
            [self.lblNoDataFoundDTV setHidden:NO];
        }
        
        if (jackpotData.count > 0) {
            [self.jackpotTableview setHidden:NO];
        }
        else{
            [self.jackpotTableview setHidden:YES];
            [self.lblNoDataFoundJTV setHidden:NO];
        }
    
        
        
        [self.hourlyTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
        
         [self.DailyTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
       
        [self.jackpotTableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
//        
//        
        
        
        
    }
    
    else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[currentBetData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}
-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag ==11){
        
        if (buttonIndex == 1) {
            
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            loginVc*lVc = [[loginVc alloc]init];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            lVc= [storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
            UINavigationController *navControllr = [[UINavigationController alloc]initWithRootViewController:lVc];
            navControllr.navigationBarHidden = true;
            delegate.window.rootViewController= navControllr;
            [delegate.window makeKeyAndVisible];
            
            
        }
    }
    
}



-(void)profileApiFromServer{
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/%@",userid];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        
        
        profileData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(profileMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
-(void)profileMessage
{
    
    if ([[profileData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        NSString*str = [NSString stringWithFormat:@"%@ %@",[profileData valueForKey:@"firstNm"] ,[profileData valueForKey:@"lastNm"]];
        _txtSideName.text = str ;
        
        NSString* mobile = [profileData valueForKey:@"mobile"];
        
        _txtSideEmail.text = mobile ;
        
        
        credits = [NSString stringWithFormat:@"%@",[profileData valueForKey:@"credits"]];
        
        [self.SideImage sd_setImageWithURL:[NSURL URLWithString:[profileData valueForKey:@"prflUrl"]]placeholderImage:[UIImage imageNamed:@"no-photo.jpg"]];

        
      
        
        [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"UserName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
        
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[profileData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}




@end
