//
//  SupportVc.h
//  FonadaApp
//
//  Created by katoch on 18/08/17.
//  Copyright © 2017 Rhythmus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "sideMenuCell.h"
#import "PlayVc.h"
#import "AboutVc.h"
#import "ContactVc.h"
#import "SideHistoryVc.h"
#import "PrivcyVc.h"
#import "TermsVc.h"
#import "FaqVc.h"
#import "lottryProfileVc.h"
#import "AppDelegate.h"
#import "loginVc.h"
#import "UIImageView+WebCache.h"


@interface SupportVc : UIViewController<UIGestureRecognizerDelegate>{
    DropDownView *dropDownView ;
    NSMutableArray*menuItemsSection1;
    NSMutableArray*sectionImages1;
       NSString *userid ;
    id currentBetData ;
    id historyData;
    
    NSArray *hourlydata;
    NSArray *dailyData;
    NSArray *jackpotData;
    
    id profileData ;
    NSString*credits;
    
}


@property(strong,nonatomic)NSString *checkStr;
@property (strong, nonatomic) UITextField *dropDownTxtfield;


@property (strong, nonatomic) IBOutlet UIView *Viewheader;

@property (strong, nonatomic) IBOutlet UIButton *btnHourly;
@property (strong, nonatomic) IBOutlet UIButton *btnDaily;
@property (strong, nonatomic) IBOutlet UIButton *btnJackpot;




        ////////    scroll MainView or SenderSubView,templateSubView, CreditsSubView

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (strong, nonatomic) IBOutlet UIView *subViewSenderID;
@property (strong, nonatomic) IBOutlet UIView *subViewTemplateID;
@property (strong, nonatomic) IBOutlet UIView *subViewCredits;

@property (strong, nonatomic) IBOutlet UILabel *lblScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblHeightConstant;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblLeadingConstant;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidthConstant;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

//////    SUBVIEW SENDER DATA
@property (strong, nonatomic) IBOutlet UILabel *lblNoDataFoundHTV;
@property (strong, nonatomic) IBOutlet UITableView *hourlyTableView;

 //////    SUBVIEW Template DATA
@property (strong, nonatomic) IBOutlet UITableView *DailyTableView;
@property (strong, nonatomic) IBOutlet UILabel *lblNoDataFoundDTV;

//////    SUBVIEW Credit DATA

@property (strong, nonatomic) IBOutlet UITableView *jackpotTableview;
@property (strong, nonatomic) IBOutlet UILabel *lblNoDataFoundJTV;




- (IBAction)hourlyClicked:(id)sender;
- (IBAction)jackpotClicked:(id)sender;
- (IBAction)dailyClicked:(id)sender;



- (IBAction)backClicked:(id)sender;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *supportTitleHeightHeaderView;



@property (strong, nonatomic) IBOutlet UIView *Viewimage;
@property (strong, nonatomic) IBOutlet UIImageView *SideImage;
@property (strong, nonatomic) IBOutlet UITableView *sideTableView;
@property (strong, nonatomic) IBOutlet UILabel *txtSideName;
@property (strong, nonatomic) IBOutlet UILabel *txtSideEmail;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSlide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
- (IBAction)menuClicked:(id)sender;


@end
