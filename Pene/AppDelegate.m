//
//  AppDelegate.m
//  Pene
//
//  Created by katoch on 01/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "AppDelegate.h"
#import "loginVc.h"
#import "ATAppUpdater.h"
#import <AFNetworking/AFNetworking.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
     NSLog(@"Success");
//    [self CheckVerionApi];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isFirst"] == false)
    {
        loginVc*lVc = [[loginVc alloc]init];
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        lVc= [storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
        UINavigationController *navControllr = [[UINavigationController alloc]initWithRootViewController:lVc];
        navControllr.navigationBarHidden = true;
        self.window.rootViewController= navControllr;
        [self.window makeKeyAndVisible];
        
        
    }
    else{
        UITabBarController *rootViewController = [[UITabBarController alloc] init];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        self.window.rootViewController = rootViewController ;
        [self.window makeKeyAndVisible];
        
    }
    
    
    
    // Override point for customization after application launch.

    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
///////

-(void)CheckVerionApi

{
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/launch/app?cnsmrId=1000"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        //        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        versionCheck = responseObject;
        [self performSelectorOnMainThread:@selector(successMessage) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        
        
    }];
    
    
}

-(void)successMessage{
    
    if ([[versionCheck valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
        NSString *bundleIdentifier = bundleInfo[@"CFBundleIdentifier"];
        NSString *currentVersion = bundleInfo[@"CFBundleShortVersionString"];
        
        
        CGFloat yourVersion = [currentVersion floatValue];
        
        CGFloat serverVersion = [[versionCheck valueForKey:@"appVersion"] floatValue];
        
        
        if (yourVersion < serverVersion ) {
            
            [[ATAppUpdater sharedUpdater] showUpdateWithConfirmation];
            
//            [[ATAppUpdater sharedUpdater] showUpdateWithForce];
            
            
        }
        else{
            
            NSLog(@"Version 1.0");
            
        }
        
    }
}

- (void)appUpdaterDidShowUpdateDialog
{
    NSLog(@"appUpdaterDidShowUpdateDialog");
}

- (void)appUpdaterUserDidLaunchAppStore
{
    NSLog(@"appUpdaterUserDidLaunchAppStore");
}

- (void)appUpdaterUserDidCancel
{
    NSLog(@"appUpdaterUserDidCancel");
}

@end
