//
//  HistoryCell.h
//  Pene
//
//  Created by katoch on 05/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *cellMainView;

@property (strong, nonatomic) IBOutlet UILabel *txtWininigNo;
@property (strong, nonatomic) IBOutlet UILabel *txtDate;
@property (strong, nonatomic) IBOutlet UILabel *txtBetNo;
@property (strong, nonatomic) IBOutlet UILabel *txtStatus;
@property (strong, nonatomic) IBOutlet UILabel *txtAmount;
@end
