//
//  HistoryVc.h
//  Pene
//
//  Created by katoch on 05/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "PlayVc.h"
#import "AboutVc.h"
#import "ContactVc.h"
#import "SideHistoryVc.h"
#import "PrivcyVc.h"
#import "TermsVc.h"
#import "FaqVc.h"
#import "lottryProfileVc.h"
#import "SupportVc.h"
#import "UIImageView+WebCache.h"

@interface HistoryVc : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>{
    DropDownView *dropDownView ;
    NSMutableArray*menuItemsSection1;
    NSMutableArray*sectionImages1;
    NSString *userid ;
    
    id rechargeData;
    id historyData;
   
    NSArray *hourlydata;
    NSArray *dailyData;
    NSArray *jackpotData;
    
    id profileData;
    NSString*credits;
    
    
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblNoDataFoundCH;
@property (strong, nonatomic) IBOutlet UILabel *lblNoDataFoundBH;

@property (strong, nonatomic) IBOutlet UITextField *txtpin1;
@property (strong, nonatomic) IBOutlet UITextField *txtPin2;
@property (strong, nonatomic) IBOutlet UITextField *txtPin3;
@property (strong, nonatomic) IBOutlet UITextField *txtPin4;



@property (strong, nonatomic) UITextField *dropDownTxtfield;

@property (strong, nonatomic) IBOutlet UILabel *txtAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnAddCredit;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xlbl;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xScroll;
- (IBAction)creditHistoryClicked:(id)sender;
- (IBAction)betHistoryClicked:(id)sender;
- (IBAction)addCreditClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *creditTableView;
@property (strong, nonatomic) IBOutlet UITableView *betTableView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btnCreditHistory;
@property (strong, nonatomic) IBOutlet UIButton *btnBetHistory;



@property (strong, nonatomic) IBOutlet UIView *Viewimage;
@property (strong, nonatomic) IBOutlet UIImageView *SideImage;
@property (strong, nonatomic) IBOutlet UITableView *sideTableView;
@property (strong, nonatomic) IBOutlet UILabel *txtSideName;
@property (strong, nonatomic) IBOutlet UILabel *txtSideEmail;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSlide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
- (IBAction)menuClicked:(id)sender;
- (IBAction)buyClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnBuy;
@property (strong, nonatomic) IBOutlet UITextField *txtRechAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnRecharge;
- (IBAction)rechargeClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewReharge;
- (IBAction)backRecharge:(id)sender;

@property(strong,nonatomic)NSString *checkStr;

@end
