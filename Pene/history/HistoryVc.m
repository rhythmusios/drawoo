//
//  HistoryVc.m
//  Pene
//
//  Created by katoch on 05/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "HistoryVc.h"
#import "HistoryCell.h"
#import "CreditCell.h"
#import "sideMenuCell.h"
#import <QuartzCore/QuartzCore.h>
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"
#import "AppDelegate.h"
#import "loginVc.h"
@interface HistoryVc ()

@end

@implementation HistoryVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_sideTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    _betTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    _creditTableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    
    
    UITapGestureRecognizer *gesRecogniz = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRechargeGesture)]; // Declare the Gesture.
    gesRecogniz.delegate = self;
    [self.viewReharge addGestureRecognizer:gesRecogniz];
    
    _btnAddCredit.layer.cornerRadius = 15.0f ;
    _btnAddCredit.layer.masksToBounds = YES;
    
    
    
    menuItemsSection1 = [[NSMutableArray alloc]initWithObjects:@"Profile",@"About us",@"Contact us",@"History",@"Privacy Policy",@"Terms & Conditions",@"Logout",nil];
 sectionImages1 = [[NSMutableArray alloc]initWithObjects:@"avatar",@"aboutUs",@"contactUs",@"history-symbol-of-antique-building (3)",@"privacy-60",@"terms",@"aboutUs",@"logout-60", nil];
    
    
    
    _SideImage.layer.cornerRadius = self.SideImage.frame.size.width/2 ;
    _SideImage.layer.masksToBounds = YES;
    
    _Viewimage.layer.cornerRadius = self.Viewimage.frame.size.width/2 ;
    _Viewimage.layer.masksToBounds = YES;
    _scrollView.delegate = self ;
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.viewMenuSlide addGestureRecognizer:gesRecognizer4];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self.lblNoDataFoundBH setHidden:YES];
  //  [self.lblNoDataFoundCH setHidden:YES];
    
    
    [self.creditTableView setHidden:YES];
    userid =   [[NSUserDefaults standardUserDefaults]valueForKey:@"id"];

    _viewReharge.hidden = YES ;
    _btnRecharge.layer.cornerRadius = 15.0 ;
    _btnRecharge.layer.masksToBounds = 15.0f ;
    
    
    _viewMenuSlide.hidden = true;
    _xScroll.constant = 0 ;
    [self.view layoutIfNeeded];
    
    [self profileApiFromServer];
    
    
    [KVNProgress show];
    [self   HistoryBidsGetApi];
    
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    

        NSInteger pagenumber = _scrollView.contentOffset.x / _scrollView.bounds.size.width;
        
        NSLog(@"Page number is %li", (long)pagenumber);
        if (pagenumber==1) {
            
             _xlbl.constant = self.view.frame.size.width/2 ;
 
            
            
        }else if (pagenumber==0)
        {
            _xlbl.constant = 0 ;


            
        }
        
     [self.view layoutIfNeeded];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
   
        return 1;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView== _betTableView) {
        
        return [[historyData valueForKey:@"dataLst"] count];
        
       
    }   else if (tableView== _creditTableView) {
        
        return 0;
        
    } else if (tableView== _sideTableView) {
        
        return menuItemsSection1.count;
        
    }
    else
    {
        
        return 0 ;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (tableView == _sideTableView) {
        
        
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            return 45;
        }else{
            return 65 ;
            
        }
        
    }else{
        
        return 51 ;
    
    }
    
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (tableView == _betTableView) {
            HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
            if (cell==nil) {
                NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"HistoryCell" owner:self options:nil];
    
                cell= arr[0];
    
            }
          
           
            if(indexPath.row % 2 == 0)
            {
                cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
            }
            else
            {
                cell.backgroundColor = [UIColor clearColor];
            }
                                 
                                 
            
       NSString*winStr =  [NSString stringWithFormat:@"%@" ,[[[historyData valueForKey:@"dataLst"]valueForKey:@"winNo"]objectAtIndex:indexPath.row]];
                                 
            
            if ([winStr isEqualToString:@"<null>"] || winStr == nil) {
                cell.txtWininigNo.text  = @"Scheduled";
            }
            else{
                cell.txtWininigNo.text  = winStr ;
                
            }
            
            NSString*BidEndStr =  [NSString stringWithFormat:@"%@" ,[[[historyData valueForKey:@"dataLst"]valueForKey:@"drawTym"]objectAtIndex:indexPath.row]];
            
            NSInteger bidend = [BidEndStr integerValue];
            NSDate *drwDate = [NSDate dateWithTimeIntervalSince1970:(bidend / 1000.0)];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd-MMM-yyyy"];
            //        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            NSString *BidDrawDate = [formatter stringFromDate:drwDate];
            cell.txtDate .text =  BidDrawDate ;
            
            
            
            cell.txtAmount.text = [NSString stringWithFormat:@"%@" ,[[[historyData valueForKey:@"dataLst"]valueForKey:@"bidAmt"]objectAtIndex:indexPath.row]];
            
            
            cell.txtStatus.text = [NSString stringWithFormat:@"%@" ,[[[historyData valueForKey:@"dataLst"]valueForKey:@"result"]objectAtIndex:indexPath.row]];
            
            
            cell.txtBetNo.text = [NSString stringWithFormat:@"%@" ,[[[historyData valueForKey:@"dataLst"]valueForKey:@"bidNo"]objectAtIndex:indexPath.row]];
            
    
            return cell;
    
        } else if (tableView == _creditTableView) {
            CreditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreditCell"];
            if (cell==nil) {
                NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"CreditCell" owner:self options:nil];
                
                cell= arr[0];
                
            }
            
            if(indexPath.row % 2 == 0)
            {
                cell.backgroundColor = [UIColor colorWithRed:224 green:224 blue:224 alpha:1.0];
            }
            else
            {
                cell.backgroundColor = [UIColor clearColor];
            }
            
            
            
            return cell;
            
        }
    if (tableView== _sideTableView){
        sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
        
        cell.txtMenu.text  = [menuItemsSection1 objectAtIndex:indexPath.row];
        cell.menuImg.image = [UIImage imageNamed:[sectionImages1 objectAtIndex:indexPath.row]];
        
        
      
        
        return cell;


    }
    return 0;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_sideTableView) {
        
        if ([menuItemsSection1[indexPath.row] isEqualToString:@"Profile"]) {
            lottryProfileVc*tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"lottryProfileVc"];
            
            
            
            [self showViewController:tbc sender:self];
            
            
        }
        //        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"How to play"])
        //        {
        //            PlayVc *play = [[PlayVc alloc]init];
        //            play = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVc"];
        //
        //
        //
        //  [self showViewController:play sender:self];
        //
        //
        //        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"About us"])
        {
            AboutVc *About = [[AboutVc alloc]init];
            About = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutVc"];
            
            [self showViewController:About sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Contact us"])
        {
            ContactVc *contact = [[ContactVc alloc]init];
            contact = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactVc"];
            
            [self showViewController:contact sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"History"])
        {
            SideHistoryVc *his = [[SideHistoryVc alloc]init];
            his = [self.storyboard instantiateViewControllerWithIdentifier:@"SideHistoryVc"];
            his.checkStr = @"History";
            
            [self showViewController:his sender:self];
            
        }
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Privacy Policy"])
        {
            PrivcyVc *privac = [[PrivcyVc alloc]init];
            privac = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivcyVc"];
            
            [self showViewController:privac sender:self];
            
        }else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Terms & Conditions"])
        {
            TermsVc *term = [[TermsVc alloc]init];
            term = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsVc"];
            
            [self showViewController:term sender:self];
            
        }
        //        else if (indexPath.row == 7)
        //        {
        //            FaqVc *About = [[FaqVc alloc]init];
        //            About = [self.storyboard instantiateViewControllerWithIdentifier:@"FaqVc"];
        //
        //            [self showViewController:About sender:self];
        //
        //        }
        
        
        else if ([menuItemsSection1[indexPath.row] isEqualToString:@"Logout"]) {
            UIAlertView*logout = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure you want to log out" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            logout.tag = 11 ;
            logout.delegate = self;
            [logout show];
        }
        
    }
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag ==11){
        
        if (buttonIndex == 1) {
            
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            loginVc*lVc = [[loginVc alloc]init];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            lVc= [storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
            UINavigationController *navControllr = [[UINavigationController alloc]initWithRootViewController:lVc];
            navControllr.navigationBarHidden = true;
            delegate.window.rootViewController= navControllr;
            [delegate.window makeKeyAndVisible];
            
            
        }
    }
    
}

- (IBAction)creditHistoryClicked:(id)sender {
    [_scrollView setContentOffset:CGPointMake(0, 0)animated:YES];
    _xlbl.constant = 0 ;
    
    [self.view layoutIfNeeded];
}

- (IBAction)betHistoryClicked:(id)sender {
    
    [_scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width, 0)animated:YES];

    _xlbl.constant = self.view.frame.size.width/2 ;
     [self.view layoutIfNeeded];
    
}



- (IBAction)addCreditClicked:(id)sender {
    
    _txtRechAmount.text = @"";
      _viewReharge.hidden = NO ;
    

    
}

- (IBAction)menuClicked:(id)sender {
  
    
    [self.view bringSubviewToFront:self.viewMenuSlide];
    _viewMenuSlide.hidden = false;
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = 0;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        //            _viewMenuSlide.hidden = true;
        
    }];
    
}

- (IBAction)buyClicked:(id)sender {
}

- (IBAction)cancelClicked:(id)sender {
}

-(void)tapGesture{
    
    [dropDownView closeAnimation];
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
        _viewMenuSlide.hidden = true;
        
    }];
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }else if ([touch.view isDescendantOfView:_sideTableView]) {
        
        
        
        return NO;
    }
    
    
    return YES;
}

- (IBAction)rechargeClicked:(id)sender {
    if ([_txtRechAmount.text isEqualToString:@""]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your recharge amount"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        [KVNProgress show];
        [self  RechargeGetApiFromServer];
        
        
    }
}

-(void)RechargeGetApiFromServer{
    
    

    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/recharge?credits=%@&id=%@",_txtRechAmount.text ,userid];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         rechargeData = responseObject;
         
         [self performSelectorOnMainThread:@selector(rechargeData) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              UIAlertController * alert = [UIAlertController
                                           alertControllerWithTitle:nil
                                           message:@"Server Error"
                                           preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction* noButton = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             
                                         }];
              
              [alert addAction:noButton];
              [self presentViewController:alert animated:YES completion:nil];
              
              
              
          }];
    
    
    
}

-(void)rechargeData{
    
  
    
    
    if ([[rechargeData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        [self profileApiFromServer];
      
        NSString *phNo = @"*121#";
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            UIAlertView*  calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [calert show];
        }
        
        
    
            
            
        }else{
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:[rechargeData valueForKey:@"message"]
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        
    }
-(void)kvndismiss{
    
    [KVNProgress dismiss];
    
}




- (IBAction)backRecharge:(id)sender {
    [self.viewReharge setHidden:YES];
}

-(void)tapRechargeGesture{
      [self.viewReharge setHidden:YES];
    
}



///////////history api //////////////


-(void)HistoryBidsGetApi{
    //    http://52.172.26.198:8082/drawoapp//secure/drw/bids/1000?page=0&size=10
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp//secure/drw/bids/%@?page=0&size=10",userid];
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject)
     
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
         
         historyData = responseObject;
         
         [self performSelectorOnMainThread:@selector(historyData) withObject:nil waitUntilDone:YES];
         
         
     }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              UIAlertController * alert = [UIAlertController
                                           alertControllerWithTitle:nil
                                           message:@"Server Error"
                                           preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction* noButton = [UIAlertAction
                                         actionWithTitle:@"Ok"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             
                                         }];
              
              [alert addAction:noButton];
              [self presentViewController:alert animated:YES completion:nil];
              
              
              
          }];
    
    
    
}

-(void)historyData{
    
    
    
    
    if ([[historyData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        
        NSLog(@"%@",historyData);
        
        
        
        
        if ([[historyData valueForKey:@"dataLst"] count] > 0) {
            NSString*hourly = @"HOURLY" ;
            
            [self.betTableView setHidden:NO];
            
            NSPredicate *predicate = [NSPredicate
                                      predicateWithFormat:@"SELF.schdlTyp contains %@",
                                      hourly];
            
            hourlydata = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicate];
            
            
            
            NSString*DAILY = @"DAILY" ;
            
            NSPredicate *predicateDaily = [NSPredicate
                                           predicateWithFormat:@"SELF.schdlTyp contains %@",
                                           DAILY];
            
            dailyData = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predicateDaily];
            
            
            
            NSString*JACKPOT = @"JACKPOT" ;
            NSPredicate *predJACKPOT = [NSPredicate
                                        predicateWithFormat:@"SELF.schdlTyp contains %@",
                                        JACKPOT];
            
            jackpotData = [[historyData valueForKey:@"dataLst"] filteredArrayUsingPredicate:predJACKPOT];
            
            
            
           
            
            [self.betTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }
        else{
            
            [self.betTableView setHidden:YES];
            
            [self.lblNoDataFoundBH setHidden:NO];
            
        }
        
        
        
        
        
        
        
        
    }
    
    else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[historyData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



-(void)profileApiFromServer{
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://52.172.26.198:8082/drawoapp/secure/usr/%@",userid];
    
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
        
        
        profileData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(profileMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvndismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
-(void)profileMessage
{
    
    if ([[profileData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        NSString*str = [NSString stringWithFormat:@"%@ %@",[profileData valueForKey:@"firstNm"] ,[profileData valueForKey:@"lastNm"]];
        _txtSideName.text = str ;
        
        NSString* mobile = [profileData valueForKey:@"mobile"];
        
        _txtSideEmail.text = mobile ;
        
        
        credits = [NSString stringWithFormat:@"%@",[profileData valueForKey:@"credits"]];
        
        NSInteger  valueCredit = [credits integerValue];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        
        NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithInteger:valueCredit]];
        NSLog(@"the Formatted String is  %@",formatted);
        
        
        
        
        _txtAmount.text = [NSString stringWithFormat:@"TZS %@",formatted];
        
         [self.SideImage sd_setImageWithURL:[NSURL URLWithString:[profileData valueForKey:@"prflUrl"]]placeholderImage:[UIImage imageNamed:@"no-photo.jpg"]];
        
        
        [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"UserName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:mobile forKey:@"mobile"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[profileData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}


@end
