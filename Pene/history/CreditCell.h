//
//  CreditCell.h
//  Pene
//
//  Created by katoch on 05/03/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *txtDate;
@property (strong, nonatomic) IBOutlet UILabel *txtAmount;
@property (strong, nonatomic) IBOutlet UILabel *txtOperator;
@property (strong, nonatomic) IBOutlet UILabel *txtStatus;

@end
